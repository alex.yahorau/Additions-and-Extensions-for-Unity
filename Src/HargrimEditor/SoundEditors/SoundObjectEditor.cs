﻿using Hargrim.Sound.SoundProviderStuff;
using System;
using UnityEditor;
using UnityEngine;

namespace HargrimEditor.SoundEditors
{
    internal abstract class SoundObjectEditor : Editor
    {
        private SoundObjectInfo m_target;
        private string m_length;

        private void OnEnable()
        {
            m_target = target as SoundObjectInfo;
            m_length = TimeSpan.FromSeconds(m_target.AudioSource.clip.length).ToString(@"hh\:mm\:ss\:fff");
        }

        public override void OnInspectorGUI()
        {
            if (m_target.AudioSource.isPlaying)
            {
                GUILayout.Space(10f);
                EditorGUILayout.LabelField("Clip: " + m_target.ClipName);
                EditorGUILayout.LabelField("Length: " + m_length);

                GUI.enabled = false;
                EditorGUILayout.Slider(m_target.AudioSource.time, 0f, m_target.AudioSource.clip.length);
                GUI.enabled = true;

                if (EditorScriptUtility.DrawCenterButton("Stop", 30f, 150f))
                    m_target.Stop();

                GUILayout.Space(10f);
            }
        }
    }
}
