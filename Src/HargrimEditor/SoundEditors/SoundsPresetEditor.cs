﻿using UnityObject = UnityEngine.Object;
using System;
using UnityEngine;
using UnityEditor;
using Hargrim.Sound.SoundProviderStuff;
using Hargrim.MathExt;

namespace HargrimEditor.SoundEditors
{
    [CustomEditor(typeof(SoundsPreset))]
    internal class SoundsPresetEditor : Editor
    {
        private SerializedProperty m_nodes;

        private Vector2 m_scrollPos;
        private bool m_sure;

        private void OnEnable()
        {
            m_nodes = serializedObject.FindProperty("m_nodes");
        }

        public override void OnInspectorGUI()
        {
            int length = m_nodes.arraySize;

            m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, false, false, GUILayout.MinHeight(10f), GUILayout.MaxHeight((length + 1) * 23f));
            {
                if (length > 0)
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField("Clip", EditorStyles.boldLabel, GUILayout.Width(100f));
                        EditorGUILayout.LabelField("Vol", EditorStyles.boldLabel, GUILayout.Width(40f));
                        EditorGUILayout.LabelField("Loop", EditorStyles.boldLabel, GUILayout.Width(35f));
                        EditorGUILayout.LabelField("Pitch", EditorStyles.boldLabel, GUILayout.Width(40f));
                        GUILayout.Space(5f);
                        EditorGUILayout.LabelField("Min/Max Dist", EditorStyles.boldLabel, GUILayout.Width(90f));
                    }
                    EditorGUILayout.EndHorizontal();
                }

                GUILayout.Space(5f);

                for (int i = 0; i < length; i++)
                {
                    SerializedProperty node = m_nodes.GetArrayElementAtIndex(i);

                    SerializedProperty clip = node.FindPropertyRelative("Clip");
                    SerializedProperty stats = node.FindPropertyRelative("Stats");

                    SerializedProperty volume = stats.FindPropertyRelative("Volume");
                    SerializedProperty loop = stats.FindPropertyRelative("Looped");
                    SerializedProperty pitch = stats.FindPropertyRelative("Pitch");
                    SerializedProperty minDist = stats.FindPropertyRelative("MinDist");
                    SerializedProperty maxDist = stats.FindPropertyRelative("MaxDist");

                    EditorGUILayout.BeginHorizontal();
                    {
                        GUI.enabled = false;
                        EditorGUILayout.ObjectField(clip.objectReferenceValue, typeof(AudioClip), false, GUILayout.Width(100f));
                        GUI.enabled = true;

                        volume.floatValue = EditorGUILayout.FloatField(volume.floatValue, GUILayout.Width(40f)).Saturate();
                        GUILayout.Space(10f);
                        loop.boolValue = EditorGUILayout.Toggle(loop.boolValue, GUILayout.Width(25f));
                        pitch.floatValue = EditorGUILayout.FloatField(pitch.floatValue, GUILayout.Width(40f)).Clamp(0f, 3f);
                        GUILayout.Space(5f);
                        minDist.floatValue = EditorGUILayout.FloatField(minDist.floatValue, GUILayout.Width(40f)).Clamp(0f, maxDist.floatValue - 1f);
                        maxDist.floatValue = EditorGUILayout.FloatField(maxDist.floatValue, GUILayout.Width(40f)).CutBefore(minDist.floatValue + 1f);

                        if (GUILayout.Button("X", GUILayout.Height(15f), GUILayout.Width(20f)))
                        {
                            m_nodes.DeleteArrayElementAtIndex(i);
                            break;
                        }
                    }
                    EditorGUILayout.EndHorizontal();

                    GUILayout.Space(3f);
                }
            }
            GUILayout.EndScrollView();

            GUILayout.Space(5f);

            var objects = EditorScriptUtility.DrawDropArea("Drag and drop your audio clips here.", 50f);

            if (objects != null)
            {
                f_addObjects(objects);

                f_makeFine();

                serializedObject.ApplyModifiedProperties();

                return;
            }

            GUILayout.Space(5f);

            if (m_nodes.arraySize > 0)
            {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Clear List") && m_sure)
                {
                    m_sure = false;
                    m_nodes.ClearArray();
                }
                EditorGUILayout.LabelField("I'm sure:", GUILayout.MaxWidth(55f));
                m_sure = EditorGUILayout.Toggle(m_sure, GUILayout.MaxWidth(20f));
                EditorGUILayout.EndHorizontal();
            }

            if (GUI.changed)
            {
                serializedObject.ApplyModifiedProperties();
            }
        }

        private void f_addObjects(UnityObject[] objects)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i] is AudioClip)
                {
                    m_nodes.InsertArrayElementAtIndex(m_nodes.arraySize);

                    SerializedProperty node = m_nodes.GetArrayElementAtIndex(m_nodes.arraySize - 1);

                    SerializedProperty clip = node.FindPropertyRelative("Clip");
                    SerializedProperty stats = node.FindPropertyRelative("Stats");

                    SerializedProperty volume = stats.FindPropertyRelative("Volume");
                    SerializedProperty pitch = stats.FindPropertyRelative("Pitch");
                    SerializedProperty minDist = stats.FindPropertyRelative("MinDist");
                    SerializedProperty maxDist = stats.FindPropertyRelative("MaxDist");

                    clip.objectReferenceValue = objects[i];
                    volume.floatValue = 1f;
                    pitch.floatValue = 1f;
                    minDist.floatValue = 1f;
                    maxDist.floatValue = 500f;
                }
            }
        }

        private void f_makeFine()
        {
            int i = 0;
            while (i < m_nodes.arraySize)
            {
                SerializedProperty clip = m_nodes.GetArrayElementAtIndex(i).FindPropertyRelative("Clip");

                if (clip.objectReferenceValue == null)
                {
                    m_nodes.DeleteArrayElementAtIndex(i);
                }
                else
                {
                    i++;
                }
            }

            m_nodes.SortArray(prop => (prop.FindPropertyRelative("Clip").objectReferenceValue as AudioClip).name);

            i = 0;
            while (i < m_nodes.arraySize - 1)
            {
                SerializedProperty a = m_nodes.GetArrayElementAtIndex(i).FindPropertyRelative("Clip");
                SerializedProperty b = m_nodes.GetArrayElementAtIndex(i + 1).FindPropertyRelative("Clip");

                if (a.objectReferenceValue == b.objectReferenceValue)
                {
                    m_nodes.DeleteArrayElementAtIndex(i + 1);
                }
                else
                {
                    i++;
                }
            }
        }
    }
}
