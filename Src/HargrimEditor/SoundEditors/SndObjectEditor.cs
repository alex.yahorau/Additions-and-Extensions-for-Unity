﻿using Hargrim.Sound.SoundProviderStuff;
using UnityEditor;

namespace HargrimEditor.SoundEditors
{
    [CustomEditor(typeof(SndObject))]
    internal class SndObjectEditor : SoundObjectEditor { }
}
