﻿using HargrimEditor.Windows;
using UnityEditor;

namespace HargrimEditor
{
    internal static class MenuItems
    {
        [MenuItem(EditorScriptUtility.Category + "/Sound/Create Sounds Preset")]
        private static void CreateSoundsPreset()
        {
            EditorScriptUtility.CreateAsset("SoundsPreset");
        }

        [MenuItem(EditorScriptUtility.Category + "/Sound/Create Music Preset")]
        private static void CreateMusicPreset()
        {
            EditorScriptUtility.CreateAsset("MusicPreset");
        }

        [MenuItem(EditorScriptUtility.Category + "/Input/Create Input Layout Config")]
        private static void CreateLayoutSet()
        {
            EditorScriptUtility.CreateAsset("LayoutConfig", "LayoutConfig");
        }

        [MenuItem(EditorScriptUtility.Category + "/Input/Gamepad Axes")]
        private static void GetAxisCreateWindow()
        {
            EditorWindow.GetWindow(typeof(AxisCreateWindow), true, "Gamepad Axes");
        }

        [MenuItem(EditorScriptUtility.Category + "/GameObjects/Create Rect Plane")]
        private static void GetCreateRectPlaneWizard()
        {
            ScriptableWizard.DisplayWizard("Create Rect Plane", typeof(CreateRectPlaneWizard));
        }

        [MenuItem(EditorScriptUtility.Category + "/GameObjects/Create Figured Plane")]
        private static void GetCreateFiguredPlaneWizard()
        {
            ScriptableWizard.DisplayWizard("Create Figured Plane", typeof(CreateFiguredPlaneWizard));
        }

        [MenuItem(EditorScriptUtility.Category + "/GameObjects/Create Shape")]
        private static void GetCreateShapeWizard()
        {
            ScriptableWizard.DisplayWizard("Create Shape", typeof(CreateShapeWizard));
        }

        [MenuItem(EditorScriptUtility.Category + "/Assets/Create Scriptable Object")]
        private static void GetScriptableObjectWindow()
        {
            EditorWindow.GetWindow(typeof(ScriptableObjectWindow), true, "Scriptable Objects");
        }

        // ------------- //

        [MenuItem(EditorScriptUtility.Category + "/About", false, 50)]
        private static void GetAboutWindow()
        {
            EditorWindow.GetWindow(typeof(AboutWindow), true, "About");
        }
    }
}
