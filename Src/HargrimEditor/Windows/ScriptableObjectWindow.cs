﻿using System;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Linq;

namespace HargrimEditor.Windows
{
    internal class ScriptableObjectWindow : EditorWindow
    {
        private static bool m_keepOpened;

        private string[] m_types;
        private int m_index;

        private void OnEnable()
        {
            minSize = new Vector2(300f, 100f);
            maxSize = new Vector2(300f, 100f);

            Assembly[] assemblies = EditorScriptUtility.GetAssemblies();
            Type[] types = EditorScriptUtility.GetTypes(assemblies, f_select);
            m_types = types.Select(itm => itm.Name).ToArray();
        }

        private void OnGUI()
        {
            if (m_types.Length == 0)
            {
                EditorGUILayout.LabelField("There is no any ScriptableObject inheritor.");
            }
            else
            {
                GUILayout.Space(10f);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("ScriptableObject:", EditorStyles.boldLabel, GUILayout.MinWidth(10f));
                m_index = EditorGUILayout.Popup(m_index, m_types);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                if (EditorScriptUtility.DrawCenterButton("Create", 50f, 30f))
                {
                    EditorScriptUtility.CreateAsset(m_types[m_index]);
                    if (!m_keepOpened)
                        Close();
                }

                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(10f);
                m_keepOpened = EditorGUILayout.Toggle(m_keepOpened, GUILayout.MaxWidth(20f));
                EditorGUILayout.LabelField("Keep opened");
                EditorGUILayout.EndHorizontal();
            }
        }

        private bool f_select(Type type)
        {
            return type.IsSubclassOf(typeof(ScriptableObject)) &&
                  !type.IsSubclassOf(typeof(Editor)) &&
                  !type.IsSubclassOf(typeof(EditorWindow));
        }
    }
}
