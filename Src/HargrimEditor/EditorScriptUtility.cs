﻿using UnityObject = UnityEngine.Object;
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using UnityEditor;
using Hargrim.Collections;

namespace HargrimEditor
{
    internal static class EditorScriptUtility
    {
        internal const string Category = "Hargrim";

        internal static Assembly[] GetAssemblies()
        {
            var files = Directory.GetFiles(@"Library\ScriptAssemblies\", "*.dll", SearchOption.AllDirectories);

            return files.Select(file => Assembly.LoadFrom(file)).ToArray();
        }

        internal static Type[] GetTypes(Assembly[] assemblies, Func<Type, bool> selector)
        {
            List<Type> types = new List<Type>();

            for (int i = 0; i < assemblies.Length; i++)
            {
                types.AddRange(assemblies[i].GetTypes());
            }

            return types.Where(selector).ToArray();
        }

        internal static void CreateAsset(string objectName, string fileName = null)
        {
            ScriptableObject so = ScriptableObject.CreateInstance(objectName);
            string name = string.Concat("Assets/", fileName.HasUsefulData() ? fileName : objectName, ".asset");
            AssetDatabase.CreateAsset(so, name);
            AssetDatabase.SaveAssets();
        }

        internal static SerializedObject GetInputManager()
        {
            string path = "ProjectSettings/InputManager.asset";
            UnityObject[] assets = AssetDatabase.LoadAllAssetsAtPath(path);
            return new SerializedObject(assets[0]);
        }

        internal static bool DrawCenterButton(string text, float w, float h)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space();
            bool pressed = GUILayout.Button(text, GUILayout.MinWidth(w), GUILayout.MaxHeight(h));
            EditorGUILayout.Space();
            EditorGUILayout.EndHorizontal();

            return pressed;
        }

        internal static void DrawCenterLabel(string text, float w)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(text, EditorStyles.boldLabel, GUILayout.Width(w));
            EditorGUILayout.Space();
            EditorGUILayout.EndHorizontal();
        }

        //The function was taken here: https://gist.github.com/bzgeb
        //God save this guy
        internal static UnityObject[] DrawDropArea(string text, float h)
        {
            Event curEvent = Event.current;
            Rect dropArea = GUILayoutUtility.GetRect(0f, h, GUILayout.ExpandWidth(true));
            GUI.Box(dropArea, text);

            switch (curEvent.type)
            {
                case EventType.DragUpdated:
                case EventType.DragPerform:
                    if (dropArea.Contains(curEvent.mousePosition))
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                        if (curEvent.type == EventType.DragPerform)
                        {
                            DragAndDrop.AcceptDrag();
                            return DragAndDrop.objectReferences;
                        }
                    }
                    break;
            }

            return null;
        }

        internal static bool EqualizeSize(SerializedProperty arrayProp, int targetSize, object defVal)
        {
            bool changed = false;

            while (arrayProp.arraySize < targetSize)
            {
                arrayProp.InsertArrayElementAtIndex(arrayProp.arraySize);
                f_initProp(arrayProp.GetArrayElementAtIndex(arrayProp.arraySize - 1), defVal);

                changed = true;
            }

            while (arrayProp.arraySize > targetSize)
            {
                arrayProp.DeleteArrayElementAtIndex(arrayProp.arraySize - 1);

                changed = true;
            }

            return changed;
        }

        private static void f_initProp(SerializedProperty property, object value)
        {
            switch (property.propertyType)
            {
                case SerializedPropertyType.Integer:
                    property.intValue = (int)value;
                    break;

                case SerializedPropertyType.Float:
                    property.floatValue = (float)value;
                    break;

                case SerializedPropertyType.Enum:
                    property.enumValueIndex = (int)value;
                    break;
            }
        }

        internal static void MoveElements(ref BitMask64 bits, int length, bool up)
        {
            if (up)
            {
                for (int i = 0; i < length; i++)
                {
                    f_runIteration(ref bits, -1, i, i > 0);
                }
            }
            else
            {
                for (int i = length - 1; i >= 0; i--)
                {
                    f_runIteration(ref bits, 1, i, i < length - 1);
                }
            }
        }

        internal static void MoveElements(SerializedProperty arrayProp, BitMask64 bits, bool up, object defVal)
        {
            int length = arrayProp.arraySize;

            if (up)
            {
                for (int i = 0; i < length; i++)
                {
                    f_runIteration(arrayProp, bits, defVal, -1, i, i > 0);
                }
            }
            else
            {
                for (int i = length - 1; i >= 0; i--)
                {
                    f_runIteration(arrayProp, bits, defVal, 1, i, i < length - 1);
                }
            }
        }

        private static void f_runIteration(ref BitMask64 bits, int offset, int i, bool condition)
        {
            if (bits.ContainsFlag(i))
            {
                bits.RemoveFlag(i);

                if (condition)
                    bits.AddFlag(i + offset);
            }
        }

        private static void f_runIteration(SerializedProperty arrayProp, BitMask64 bits, object defVal, int offset, int i, bool condition)
        {
            if (bits.ContainsFlag(i))
            {
                bits.RemoveFlag(i);

                if (condition)
                {
                    arrayProp.MoveArrayElement(i, i + offset);
                    bits.AddFlag(i + offset);
                }
                else
                {
                    f_initProp(arrayProp.GetArrayElementAtIndex(i), defVal);
                }
            }
        }
    }
}
