﻿using UnityEngine;
using UnityEditor;
using Hargrim;

namespace HargrimEditor.Drawers
{
    [CustomPropertyDrawer(typeof(Percent))]
    internal class PercentDrawer : PropertyDrawer
    {
        private SerializedProperty m_value;
        private string m_name;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (m_value == null)
            {
                m_value = property.FindPropertyRelative(default(Percent).SerFieldName);
                m_name = property.displayName + " (%)";
            }

            m_value.floatValue = EditorGUI.FloatField(position, m_name, m_value.floatValue / Percent.TO_PERCENT) * Percent.TO_PERCENT;
        }
    }
}
