﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Hargrim;
using UnityEditor;

namespace HargrimEditor.Drawers
{
    [CustomPropertyDrawer(typeof(SortingLayerIDAttribute))]
    internal class SortingLayerIDDrawer : PropertyDrawer
    {
        private string[] m_names;
        private int m_index;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (m_names == null)
            {
                if (property.propertyType != SerializedPropertyType.Integer)
                {
                    EditorGUI.LabelField(position, "Serialized field must have type of Int32.");
                    return;
                }

                m_names = SortingLayer.layers.Select(itm => itm.name).ToArray();
                m_index = SortingLayer.layers.IndexOf(itm => itm.id == property.intValue);
                if (m_index < 0)
                    m_index = 0;
            }

            m_index = EditorGUI.Popup(position, property.displayName, m_index, m_names);
            property.intValue = SortingLayer.layers[m_index].id;
        }
    }
}
