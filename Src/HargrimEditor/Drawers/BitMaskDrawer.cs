﻿using UnityEngine;
using UnityEditor;
using Hargrim.Collections;

namespace HargrimEditor.Drawers
{
    [CustomPropertyDrawer(typeof(BitMask))]
    internal class BitMaskDrawer : PropertyDrawer
    {
        private SerializedProperty m_container;
        private string[] m_names;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (m_container == null)
            {
                m_container = property.FindPropertyRelative(default(BitMask).SerFieldName);
                m_names = new string[BitMask.SIZE];
                for (int i = 0; i < m_names.Length; i++)
                    m_names[i] = i.ToString();
            }

            m_container.intValue = EditorGUI.MaskField(position, property.displayName, m_container.intValue, m_names);
        }
    }
}
