﻿using System;
using UnityEngine;
using UnityEditor;
using Hargrim.Collections;

namespace HargrimEditor.Drawers
{
    [CustomPropertyDrawer(typeof(BitMaskEnumFlagsAttribute))]
    internal class BitMaskEnumFlagsDrawer : PropertyDrawer
    {
        private class Data
        {
            public SerializedProperty Property;
            public string[] Names;

            public Data(BitMaskEnumFlagsDrawer drawer, SerializedProperty property, int size)
            {
                Array values = Enum.GetValues((drawer.attribute as BitMaskEnumFlagsAttribute).EnumType);

                if (values.Length > size)
                    throw new ArgumentException("enum values amount cannot be more than " + BitMask.SIZE.ToString());

                Names = new string[values.Length];

                foreach (Enum item in values)
                {
                    Names[item.ToInt()] = item.GetName();
                }

                Property = property;
            }
        }

        private Data m_data;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (m_data == null)
            {
                if (property.type == typeof(BitMask).Name)
                {
                    m_data = new Data(this, property.FindPropertyRelative(default(BitMask).SerFieldName), BitMask.SIZE);
                }
                else if (property.type == typeof(BitMask64).Name)
                {
                    m_data = new Data(this, property.FindPropertyRelative(default(BitMask64).SerFieldName), BitMask64.SIZE);
                }
                else
                {
                    EditorGUI.LabelField(position, "Serialized field must have type of BitMask or BitMask64.");
                    return;
                }
            }

            m_data.Property.intValue = EditorGUI.MaskField(position, property.displayName, m_data.Property.intValue, m_data.Names);
        }
    }
}
