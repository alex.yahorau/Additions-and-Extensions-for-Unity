﻿using UnityEngine;
using UnityEditor;
using Hargrim.Collections;

namespace HargrimEditor.Drawers
{
    [CustomPropertyDrawer(typeof(BitMask64))]
    internal class BitMask64Drawer : PropertyDrawer
    {
        private SerializedProperty m_container;
        private string[] m_names;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (m_container == null)
            {
                m_container = property.FindPropertyRelative(default(BitMask64).SerFieldName);
                m_names = new string[BitMask64.SIZE];
                for (int i = 0; i < m_names.Length; i++)
                    m_names[i] = i.ToString();
            }

            m_container.intValue = EditorGUI.MaskField(position, property.displayName, m_container.intValue, m_names);
        }
    }
}
