﻿using Hargrim;
using UnityEditor;
using UnityEngine;

namespace HargrimEditor.Drawers
{
    [CustomPropertyDrawer(typeof(PercentRangeAttribute))]
    internal class PercentRangeDrawer : PropertyDrawer
    {
        private SerializedProperty m_value;
        private string m_name;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (m_value == null)
            {
                if (property.propertyType != SerializedPropertyType.Generic || property.type != "Percent")
                {
                    EditorGUI.LabelField(position, "Serialized field must have type of Percent.");
                    return;
                }

                m_value = property.FindPropertyRelative(default(Percent).SerFieldName);
                m_name = property.displayName + " (%)";
            }

            var a = attribute as PercentRangeAttribute;

            m_value.floatValue = EditorGUI.Slider(position, m_name, m_value.floatValue / Percent.TO_PERCENT, a.Min, a.Max) * Percent.TO_PERCENT;
        }
    }
}
