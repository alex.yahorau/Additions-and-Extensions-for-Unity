﻿using System;
using UnityEngine;
using System.Linq;
using UnityEditor;
using Hargrim.Controls;
using Hargrim.Controls.ControlStuff;
using Hargrim.Collections;

namespace HargrimEditor.Input
{
    [CustomEditor(typeof(LayoutConfig))]
    internal class LayoutConfigEditor : Editor
    {
        private class TypeSelector
        {
            public Type[] Types;
            public string[] TypeNames;
            public int Selected;
        }

        private class TypeValue
        {
            public Type EnumType;
            public string PropName;
            public string[] EnumNames;

            public bool IsEmpty
            {
                get { return EnumType == null; }
            }

            public TypeValue(string propName)
            {
                PropName = propName;
            }

            public void SetValue(Type type)
            {
                EnumType = type;
                if (EnumType != null) { EnumNames = Enum.GetNames(EnumType); }
            }
        }

        private TypeSelector m_keyEnumTypeSel;
        private TypeSelector m_axisEnumTypeSel;

        private TypeValue m_keyEnumTypeVal = new TypeValue("m_keyEnumType");
        private TypeValue m_axisEnumTypeVal = new TypeValue("m_axisEnumType");

        private SerializedProperty m_keyIndices;
        private SerializedProperty m_axisIndices;
        private SerializedProperty m_axisRel;

        private SerializedProperty m_inputType;
        private bool m_pretty;

        private BitMask64 m_KeyToggles;
        private bool m_allKeys;

        private BitMask64 m_axisToggles;
        private bool m_allAxes;

        private void OnEnable()
        {
            f_initTypeValue(m_keyEnumTypeVal);
            f_initTypeValue(m_axisEnumTypeVal);

            m_inputType = serializedObject.FindProperty("InputType");

            m_keyIndices = serializedObject.FindProperty("KeyIndices");
            m_axisIndices = serializedObject.FindProperty("AxisIndices");
            m_axisRel = serializedObject.FindProperty("AxisRel");
        }

        public override void OnInspectorGUI()
        {
            if (m_inputType.enumValueIndex == (int)InputType.None)
            {
                f_drawTypeChoice();
            }
            else
            {
                if (m_keyEnumTypeVal.IsEmpty)
                {
                    f_drawEnumChoice(ref m_keyEnumTypeSel, m_keyEnumTypeVal, "Choose your button action enum:");
                }
                else
                {
                    if (f_chekKeys())
                        serializedObject.ApplyModifiedProperties();

                    int length = m_keyEnumTypeVal.EnumNames.Length;
                    f_drawKeys(length);
                    if (m_KeyToggles.AnyFor(length))
                        f_drawKeysButtons(length);
                }

                if (m_axisEnumTypeVal.IsEmpty)
                {
                    f_drawEnumChoice(ref m_axisEnumTypeSel, m_axisEnumTypeVal, "Choose your axis action enum:");
                }
                else
                {
                    if (f_chekAxes())
                        serializedObject.ApplyModifiedProperties();

                    int length = m_axisEnumTypeVal.EnumNames.Length;
                    f_drawAxes(length);
                    if (m_axisToggles.AnyFor(length))
                        f_drawAxesButtons(length);
                }

                if (m_keyEnumTypeVal.IsEmpty || m_axisEnumTypeVal.IsEmpty)
                {
                    GUILayout.Space(10f);
                    EditorGUILayout.HelpBox("Enumerations which represent game actions (jump, shoot, move etc.). These enums must have consecutive int values: 0, 1, 2 etc.", MessageType.Info);
                }
                else
                {
                    GUILayout.Space(20f);

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(10f);
                    if (GUILayout.Button("Print Json", GUILayout.MaxWidth(300f)))
                    {
                        object layout;

                        if (f_isKeyMouse())
                            layout = (target as LayoutConfig).ToKeyMouseBindLayout();
                        else
                            layout = (target as LayoutConfig).ToGamepadBindLayout();

                        string json = JsonUtility.ToJson(layout, m_pretty);
                        Debug.Log(json);
                    }
                    GUILayout.Space(10f);
                    m_pretty = GUILayout.Toggle(m_pretty, "Pretty", GUILayout.MaxWidth(60f));
                    EditorGUILayout.EndHorizontal();
                }
            }

            if (GUI.changed)
            {
                serializedObject.ApplyModifiedProperties();
            }
        }

        // -- //

        private bool f_isKeyMouse()
        {
            return m_inputType.enumValueIndex == (int)InputType.KeyMouse;
        }

        private void f_initTypeValue(TypeValue typeValue)
        {
            string enumName = serializedObject.FindProperty(typeValue.PropName).stringValue ?? string.Empty;
            typeValue.SetValue(Type.GetType(enumName));
        }

        private void f_drawTypeChoice()
        {
            GUILayout.Space(10f);
            EditorScriptUtility.DrawCenterLabel("Choose control type:", 150f);
            GUILayout.Space(5f);

            if (EditorScriptUtility.DrawCenterButton("Keyboard and Mouse", 150f, 30f))
                m_inputType.enumValueIndex = (int)InputType.KeyMouse;

            GUILayout.Space(5f);

            if (EditorScriptUtility.DrawCenterButton("Gamepad", 150f, 30f))
                m_inputType.enumValueIndex = (int)InputType.Gamepad;
        }

        private void f_drawEnumChoice(ref TypeSelector selector, TypeValue typeValue, string label)
        {
            GUILayout.Space(10f);

            if (selector == null)
            {
                selector = new TypeSelector();
                var ass = EditorScriptUtility.GetAssemblies();
                selector.Types = EditorScriptUtility.GetTypes(ass, type => type.IsSubclassOf(typeof(Enum)));
                if (selector.Types.Length > 0)
                {
                    selector.Types = selector.Types.OrderBy(itm => itm.FullName).ToArray();
                    selector.TypeNames = selector.Types.Select(itm => itm.FullName).ToArray();
                }
            }

            EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
            if (selector.Types.Length > 0)
            {
                selector.Selected = EditorGUILayout.Popup(selector.Selected, selector.TypeNames);
                GUILayout.Space(5f);
                if (EditorScriptUtility.DrawCenterButton("Apply", 100f, 30f))
                {
                    typeValue.SetValue(selector.Types[selector.Selected]);
                    serializedObject.FindProperty(typeValue.PropName).stringValue = typeValue.EnumType.AssemblyQualifiedName;
                    selector = null;
                }
            }
            else
            {
                EditorGUILayout.HelpBox("Enum not found.", MessageType.Warning);
            }
        }

        private void f_drawKeys(int length)
        {
            GUILayout.Space(10f);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Actions", EditorStyles.boldLabel, GUILayout.MaxWidth(70f));
            EditorGUILayout.LabelField("KeyCodes", EditorStyles.boldLabel, GUILayout.MaxWidth(150f), GUILayout.MinWidth(10f));
            if (f_isKeyMouse())
            {
                EditorGUILayout.LabelField("CrossAxis", EditorStyles.boldLabel, GUILayout.MaxWidth(150f), GUILayout.MinWidth(10f));
            }

            bool all = m_KeyToggles.AllFor(length);
            m_allKeys = EditorGUILayout.Toggle(all, GUILayout.MaxWidth(20f));
            if (m_allKeys != all)
                m_KeyToggles.SetAll(m_allKeys);

            EditorGUILayout.EndHorizontal();
            GUILayout.Space(5f);

            for (int i = 0; i < length; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField(m_keyEnumTypeVal.EnumNames[i], GUILayout.MaxWidth(70f));
                SerializedProperty keyIndexItem = m_keyIndices.GetArrayElementAtIndex(i);

                if (f_isKeyMouse())
                {
                    keyIndexItem.intValue = (int)(AltKeyCode)EditorGUILayout.EnumPopup((AltKeyCode)keyIndexItem.intValue, GUILayout.MaxWidth(150f));
                    SerializedProperty axisRelItem = m_axisRel.GetArrayElementAtIndex(i);
                    axisRelItem.enumValueIndex = (int)(KeyAxisDir)EditorGUILayout.EnumPopup((KeyAxisDir)axisRelItem.enumValueIndex, GUILayout.MaxWidth(150f));
                }
                else
                {
                    keyIndexItem.intValue = (int)(GPKeyCode)EditorGUILayout.EnumPopup((GPKeyCode)keyIndexItem.intValue, GUILayout.MaxWidth(150f));
                }

                m_KeyToggles[i] = EditorGUILayout.Toggle(m_KeyToggles[i], GUILayout.MaxWidth(20f));

                EditorGUILayout.EndHorizontal();
            }

            GUILayout.Space(5f);
        }

        private void f_drawAxes(int length)
        {
            GUILayout.Space(10f);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Actions", EditorStyles.boldLabel, GUILayout.MaxWidth(70f));
            EditorGUILayout.LabelField("AxisCodes", EditorStyles.boldLabel, GUILayout.MaxWidth(150f), GUILayout.MinWidth(10f));

            bool all = m_axisToggles.AllFor(length);
            m_allAxes = EditorGUILayout.Toggle(all, GUILayout.MaxWidth(20f));
            if (m_allAxes != all)
                m_axisToggles.SetAll(m_allAxes);

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(5f);

            for (int i = 0; i < length; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField(m_axisEnumTypeVal.EnumNames[i], GUILayout.MaxWidth(70f));
                SerializedProperty axisIndexItem = m_axisIndices.GetArrayElementAtIndex(i);

                if (f_isKeyMouse())
                    axisIndexItem.intValue = (int)(KMAxisCode)EditorGUILayout.EnumPopup((KMAxisCode)axisIndexItem.intValue, GUILayout.MaxWidth(150f));
                else
                    axisIndexItem.intValue = (int)(GPAxisCode)EditorGUILayout.EnumPopup((GPAxisCode)axisIndexItem.intValue, GUILayout.MaxWidth(150f));

                m_axisToggles[i] = EditorGUILayout.Toggle(m_axisToggles[i], GUILayout.MaxWidth(20f));

                EditorGUILayout.EndHorizontal();
            }

            GUILayout.Space(5f);
        }

        private void f_drawKeysButtons(int length)
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Move Up", GUILayout.MaxWidth(150f)))
            {
                int defVal = InputEnum.GetKeyDefVal(m_inputType.enumValueIndex);
                EditorScriptUtility.MoveElements(m_keyIndices, m_KeyToggles, true, defVal);
                if (f_isKeyMouse())
                    EditorScriptUtility.MoveElements(m_axisRel, m_KeyToggles, true, (int)KeyAxisDir.None);
                EditorScriptUtility.MoveElements(ref m_KeyToggles, m_keyIndices.arraySize, true);
            }

            if (GUILayout.Button("Move Down", GUILayout.MaxWidth(150f)))
            {
                int defVal = InputEnum.GetKeyDefVal(m_inputType.enumValueIndex);
                EditorScriptUtility.MoveElements(m_keyIndices, m_KeyToggles, false, defVal);
                if (f_isKeyMouse())
                    EditorScriptUtility.MoveElements(m_axisRel, m_KeyToggles, false, (int)KeyAxisDir.None);
                EditorScriptUtility.MoveElements(ref m_KeyToggles, m_keyIndices.arraySize, false);
            }

            GUILayout.Space(10f);

            if (GUILayout.Button("Clear", GUILayout.MaxWidth(80f)))
            {
                bool isKeyMouse = f_isKeyMouse();
                int defVal = InputEnum.GetKeyDefVal(m_inputType.enumValueIndex);

                for (int i = 0; i < length; i++)
                {
                    if (m_KeyToggles[i])
                    {
                        m_keyIndices.GetArrayElementAtIndex(i).intValue = defVal;

                        if (isKeyMouse)
                            m_axisRel.GetArrayElementAtIndex(i).enumValueIndex = (int)KeyAxisDir.None;
                    }
                }

                m_KeyToggles.Clear();
            }

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(5f);
        }

        private void f_drawAxesButtons(int length)
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Move Up", GUILayout.MaxWidth(150f)))
            {
                int defVal = InputEnum.GetAxisDefVal(m_inputType.enumValueIndex);
                EditorScriptUtility.MoveElements(m_axisIndices, m_axisToggles, true, defVal);
                EditorScriptUtility.MoveElements(ref m_axisToggles, m_axisIndices.arraySize, true);
            }

            if (GUILayout.Button("Move Down", GUILayout.MaxWidth(150f)))
            {
                int defVal = InputEnum.GetAxisDefVal(m_inputType.enumValueIndex);
                EditorScriptUtility.MoveElements(m_axisIndices, m_axisToggles, false, defVal);
                EditorScriptUtility.MoveElements(ref m_axisToggles, m_axisIndices.arraySize, false);
            }

            GUILayout.Space(10f);

            if (GUILayout.Button("Clear", GUILayout.MaxWidth(80f)))
            {
                int defVal = InputEnum.GetAxisDefVal(m_inputType.enumValueIndex);

                for (int i = 0; i < length; i++)
                {
                    if (m_axisToggles[i])
                        m_axisIndices.GetArrayElementAtIndex(i).intValue = defVal;
                }

                m_axisToggles.Clear();
            }

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(5f);
        }

        private bool f_chekKeys()
        {
            int targetSize = m_keyEnumTypeVal.EnumNames.Length;

            int defVal = InputEnum.GetKeyDefVal(m_inputType.enumValueIndex);
            bool changed = EditorScriptUtility.EqualizeSize(m_keyIndices, targetSize, defVal);

            if (f_isKeyMouse())
                changed |= EditorScriptUtility.EqualizeSize(m_axisRel, targetSize, (int)KeyAxisDir.None);

            return changed;
        }

        private bool f_chekAxes()
        {
            int targetSize = m_axisEnumTypeVal.EnumNames.Length;
            int defVal = InputEnum.GetAxisDefVal(m_inputType.enumValueIndex);
            return EditorScriptUtility.EqualizeSize(m_axisIndices, targetSize, defVal);
        }
    }
}
