﻿using Hargrim.MathExt;
using System.Runtime.InteropServices;
using UnityEngine;
using static System.Math;

namespace Hargrim
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Arc3
    {
        private Arc2 m_arc;
        private float m_horAngle;
        private float m_sin;
        private float m_cos;

        public float VertAngle
        {
            get { return m_arc.Angle; }
            set { m_arc.Angle = value; }
        }

        public float Speed
        {
            get { return m_arc.Speed; }
            set { m_arc.Speed = value; }
        }

        public float Gravity
        {
            get { return m_arc.Gravity; }
            set { m_arc.Gravity = value; }
        }

        public float HorAngle
        {
            get { return m_horAngle; }
            set
            {
                m_horAngle = value;
                float angle = value.ToRadians();
                m_sin = (float)Sin(angle);
                m_cos = (float)Cos(angle);
            }
        }

        public Arc3(float vertAngle, float horAngle, float startSpeed, float gravity)
        {
            m_horAngle = horAngle;
            float angle = horAngle.ToRadians();
            m_sin = (float)Sin(angle);
            m_cos = (float)Cos(angle);
            m_arc = new Arc2(vertAngle, startSpeed, gravity);
        }

        public Arc3(Vector3 dir, float startSpeed, float gravity)
        {
            Vector2 startDir = dir.XZ().normalized;
            float angle = startDir.magnitude <= Vector2.kEpsilon ? 0f : Vector2.Angle(startDir, Vector2.right);
            m_horAngle = startDir.y > 0f ? -angle : angle;
            angle = m_horAngle.ToRadians();
            m_sin = (float)Sin(angle);
            m_cos = (float)Cos(angle);

            Vector3 floorProj = dir.XZ().normalized.To_XyZ();
            float shotAngle = Vector3.Angle(floorProj, dir);

            m_arc = new Arc2(dir.y < 0f ? -shotAngle : shotAngle, startSpeed, gravity);
        }

        public Vector3 Evaluate(float time)
        {
            Vector3 newPos = m_arc.Evaluate(time);

            if (m_horAngle != 0f)
                return newPos.GetRotated(Vector3.up, m_sin, m_cos);

            return newPos;
        }

        public static explicit operator Arc2(Arc3 arc3)
        {
            return arc3.m_arc;
        }

        public static implicit operator Arc3(Arc2 arc2)
        {
            return new Arc3 { m_arc = arc2 };
        }
    }
}
