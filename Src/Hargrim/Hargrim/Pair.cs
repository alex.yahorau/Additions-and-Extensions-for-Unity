﻿using System;

namespace Hargrim
{
    /// <summary>
    /// Defines a pair of elements that can be set or retrieved.
    /// </summary>
    /// <typeparam name="T1">The type of the first element.</typeparam>
    /// <typeparam name="T2">The type of the second elemen.</typeparam>
    [Serializable]
    public struct Pair<T1, T2>
    {
        [Obsolete("This property is deprecated. Use A instead.")]
        public T1 First
        {
            get { return A; }
            set { A = value; }
        }

        [Obsolete("This property is deprecated. Use B instead.")]
        public T2 Second
        {
            get { return B; }
            set { B = value; }
        }

        /// <summary>
        /// First element.
        /// </summary>
        public T1 A;

        /// <summary>
        /// Second element.
        /// </summary>
        public T2 B;

        public Pair(T1 a, T2 b)
        {
            A = a;
            B = b;
        }

        public override string ToString()
        {
            return string.Format("{0} | {1}", A, B);
        }
    }
}
