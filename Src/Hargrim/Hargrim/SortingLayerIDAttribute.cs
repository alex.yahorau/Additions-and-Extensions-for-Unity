﻿using System;
using UnityEngine;

namespace Hargrim
{
    [AttributeUsage(AttributeTargets.Field)]
    public class SortingLayerIDAttribute : PropertyAttribute { }
}
