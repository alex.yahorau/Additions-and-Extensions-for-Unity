using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hargrim.Collections
{
    /// <summary>
    /// Long integer bit mask. Can contain 64 flags.
    /// Supports casting to long and back.
    /// </summary>
    [Serializable]
    public struct BitMask64 : IEquatable<BitMask64>, IEnumerable<bool>
    {
        public const int SIZE = 64;

        [SerializeField, HideInInspector]
        private long m_field;

        internal string SerFieldName
        {
            get { return nameof(m_field); }
        }

        public bool None
        {
            get { return m_field == 0L; }
        }

        public bool Any
        {
            get { return m_field != 0L; }
        }

        public bool All
        {
            get { return m_field == -1L; }
        }

        public bool this[int index]
        {
            get { return ContainsFlag(index); }
            set
            {
                if (value) { AddFlag(index); }
                else { RemoveFlag(index); }
            }
        }

        public BitMask64(bool defaultValue)
        {
            m_field = defaultValue ? -1L : 0L;
        }

        public BitMask64(int flag0)
        {
            m_field = 1L << flag0;
        }

        public BitMask64(int flag0, int flag1)
        {
            m_field = 1L << flag0;
            m_field |= 1L << flag1;
        }

        public BitMask64(int flag0, int flag1, int flag2)
        {
            m_field = 1L << flag0;
            m_field |= 1L << flag1;
            m_field |= 1L << flag2;
        }

        public BitMask64(params int[] flags)
        {
            m_field = 0L;
            for (int i = 0; i < flags.Length; i++)
            {
                m_field |= 1L << flags[i];
            }
        }

        public bool AllFor(int length)
        {
            if (length > SIZE)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (length == SIZE)
                return All;

            bool ku = true;
            for (int i = 0; i < length; i++)
            {
                ku &= ContainsFlag(i);
            }
            return ku;
        }

        public bool AnyFor(int length)
        {
            if (length > SIZE)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (length == SIZE)
                return Any;

            for (int i = 0; i < length; i++)
            {
                if (ContainsFlag(i))
                    return true;
            }
            return false;
        }

        public void AddFlag(int index)
        {
            m_field |= 1L << index;
        }

        public void RemoveFlag(int index)
        {
            m_field &= ~(1L << index);
        }

        public void SwitchFlag(int index)
        {
            m_field ^= 1L << index;
        }

        public bool ContainsFlag(int index)
        {
            return (m_field & 1L << index) != 0L;
        }

        public void SetFlag(int index, bool value)
        {
            if (value) { AddFlag(index); }
            else { RemoveFlag(index); }
        }

        public void AddAll()
        {
            m_field = -1L;
        }

        public void Clear()
        {
            m_field = 0L;
        }

        public void SetAll(bool value)
        {
            if (value) { AddAll(); }
            else { Clear(); }
        }

        public void InvertAll()
        {
            m_field = ~m_field;
        }

        public int GetCount()
        {
            long val = m_field;
            val -= (val >> 1) & 0x5555555555555555;
            val = ((val >> 2) & 0x3333333333333333) + (val & 0x3333333333333333);
            val = ((((val >> 4) + val) & 0x0F0F0F0F0F0F0F0F) * 0x0101010101010101) >> 56;
            return (int)val;
        }

        public int GetCount(int length)
        {
            if (length > SIZE)
                throw new ArgumentOutOfRangeException(nameof(length));

            int count = 0;
            for (int i = 0; i < length; i++)
            {
                if (ContainsFlag(i))
                    count++;
            }
            return count;
        }

        public BitArray ToBitArray()
        {
            return new BitArray(m_field.GetBytes());
        }

        #region regular stuff
        public override int GetHashCode()
        {
            return m_field.GetHashCode();
        }

        public bool Equals(BitMask64 obj)
        {
            return this == obj;
        }

        public override bool Equals(object obj)
        {
            return obj is BitMask64 && this == (BitMask64)obj;
        }

        public override string ToString()
        {
            return m_field.ToString();
        }

        public IEnumerator<bool> GetEnumerator()
        {
            for (int i = 0; i < SIZE; i++)
            {
                yield return ContainsFlag(i);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        //--//

        public static implicit operator BitMask64(long value)
        {
            return new BitMask64 { m_field = value };
        }

        public static implicit operator long(BitMask64 value)
        {
            return value.m_field;
        }

        public static implicit operator BitMask64(BitMask value)
        {
            return new BitMask64 { m_field = value };
        }

        public static bool operator ==(BitMask64 a, BitMask64 b)
        {
            return a.m_field == b.m_field;
        }

        public static bool operator !=(BitMask64 a, BitMask64 b)
        {
            return a.m_field != b.m_field;
        }

        public static BitMask64 operator &(BitMask64 a, BitMask64 b)
        {
            a.m_field &= b.m_field;
            return a;
        }

        public static BitMask64 operator |(BitMask64 a, BitMask64 b)
        {
            a.m_field |= b.m_field;
            return a;
        }

        public static BitMask64 operator ^(BitMask64 a, BitMask64 b)
        {
            a.m_field ^= b.m_field;
            return a;
        }

        public static BitMask64 operator ~(BitMask64 a)
        {
            a.m_field = ~a.m_field;
            return a;
        }
    }
}
