using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hargrim.Collections
{
    /// <summary>
    /// Integer bit mask. Can contain 32 flags.
    /// Supports casting to int and back.
    /// </summary>
    [Serializable]
    public struct BitMask : IEquatable<BitMask>, IEnumerable<bool>
    {
        public const int SIZE = 32;

        [SerializeField, HideInInspector]
        private int m_field;

        internal string SerFieldName
        {
            get { return nameof(m_field); }
        }

        public bool None
        {
            get { return m_field == 0; }
        }

        public bool Any
        {
            get { return m_field != 0; }
        }

        public bool All
        {
            get { return m_field == -1; }
        }

        public bool this[int index]
        {
            get { return ContainsFlag(index); }
            set
            {
                if (value) { AddFlag(index); }
                else { RemoveFlag(index); }
            }
        }

        public BitMask(bool defaultValue)
        {
            m_field = defaultValue ? -1 : 0;
        }

        public BitMask(int flag0)
        {
            m_field = 1 << flag0;
        }

        public BitMask(int flag0, int flag1)
        {
            m_field = 1 << flag0;
            m_field |= 1 << flag1;
        }

        public BitMask(int flag0, int flag1, int flag2)
        {
            m_field = 1 << flag0;
            m_field |= 1 << flag1;
            m_field |= 1 << flag2;
        }

        public BitMask(params int[] flags)
        {
            m_field = 0;
            for (int i = 0; i < flags.Length; i++)
            {
                m_field |= 1 << flags[i];
            }
        }

        public bool AllFor(int length)
        {
            if (length > SIZE)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (length == SIZE)
                return All;

            bool ku = true;
            for (int i = 0; i < length; i++)
            {
                ku &= ContainsFlag(i);
            }
            return ku;
        }

        public bool AnyFor(int length)
        {
            if (length > SIZE)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (length == SIZE)
                return Any;

            for (int i = 0; i < length; i++)
            {
                if (ContainsFlag(i))
                    return true;
            }
            return false;
        }

        public void AddFlag(int index)
        {
            m_field |= 1 << index;
        }

        public void RemoveFlag(int index)
        {
            m_field &= ~(1 << index);
        }

        public void SwitchFlag(int index)
        {
            m_field ^= 1 << index;
        }

        public bool ContainsFlag(int index)
        {
            return (m_field & 1 << index) != 0;
        }

        public void SetFlag(int index, bool value)
        {
            if (value) { AddFlag(index); }
            else { RemoveFlag(index); }
        }

        public void AddAll()
        {
            m_field = -1;
        }

        public void Clear()
        {
            m_field = 0;
        }

        public void SetAll(bool value)
        {
            if (value) { AddAll(); }
            else { Clear(); }
        }

        public void InvertAll()
        {
            m_field = ~m_field;
        }

        public int GetCount()
        {
            int val = m_field;
            val -= (val >> 1) & 0x55555555;
            val = ((val >> 2) & 0x33333333) + (val & 0x33333333);
            val = ((((val >> 4) + val) & 0x0F0F0F0F) * 0x01010101) >> 24;
            return val;
        }

        public int GetCount(int length)
        {
            if (length > SIZE)
                throw new ArgumentOutOfRangeException(nameof(length));

            int count = 0;
            for (int i = 0; i < length; i++)
            {
                if (ContainsFlag(i))
                    count++;
            }
            return count;
        }

        public BitArray ToBitArray()
        {
            return new BitArray(m_field.GetBytes());
        }

        public LayerMask ToLayerMask()
        {
            return m_field;
        }

        #region regular stuff
        public override int GetHashCode()
        {
            return m_field.GetHashCode();
        }

        public bool Equals(BitMask obj)
        {
            return this == obj;
        }

        public override bool Equals(object obj)
        {
            return obj is BitMask && this == (BitMask)obj;
        }

        public override string ToString()
        {
            return m_field.ToString();
        }

        public IEnumerator<bool> GetEnumerator()
        {
            for (int i = 0; i < SIZE; i++)
            {
                yield return ContainsFlag(i);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        //--//

        public static implicit operator BitMask(int value)
        {
            return new BitMask { m_field = value };
        }

        public static implicit operator int(BitMask value)
        {
            return value.m_field;
        }

        public static explicit operator BitMask(BitMask64 value)
        {
            return new BitMask { m_field = (int)value };
        }

        public static bool operator ==(BitMask a, BitMask b)
        {
            return a.m_field == b.m_field;
        }

        public static bool operator !=(BitMask a, BitMask b)
        {
            return a.m_field != b.m_field;
        }

        public static BitMask operator &(BitMask a, BitMask b)
        {
            a.m_field &= b.m_field;
            return a;
        }

        public static BitMask operator |(BitMask a, BitMask b)
        {
            a.m_field |= b.m_field;
            return a;
        }

        public static BitMask operator ^(BitMask a, BitMask b)
        {
            a.m_field ^= b.m_field;
            return a;
        }

        public static BitMask operator ~(BitMask a)
        {
            a.m_field = ~a.m_field;
            return a;
        }
    }
}
