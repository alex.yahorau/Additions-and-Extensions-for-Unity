﻿namespace Hargrim
{
    public sealed class UnsupportedValueException : System.Exception
    {
        public UnsupportedValueException(object value) : base(string.Format("Unsupported value for {1}: {0}", value, value.GetType())) { }
    }
}
