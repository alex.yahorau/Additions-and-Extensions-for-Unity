﻿namespace Hargrim.Controls
{
    public interface InputObtainer
    {
        ButtonState GetKeyState(int keyAction);
        float GetAxisValue(int axisAction);
        void Update();
        void Reset();
    }
}
