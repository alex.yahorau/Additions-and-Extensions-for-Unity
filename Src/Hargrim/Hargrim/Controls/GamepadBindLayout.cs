﻿using Hargrim.Controls.ControlStuff;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Hargrim.Controls
{
    [Serializable]
    public sealed class GamepadBindLayout
    {
        [SerializeField]
        internal int[] Keys;
        [SerializeField]
        internal int[] Axes;

        private List<int> m_tmpButtons;

        public GamepadBindLayout(int[] keyIndices, int[] axisIndices)
        {
            Keys = keyIndices;
            Axes = axisIndices;
        }

        internal GamepadBindLayout(LayoutConfig config)
        {
            InputType targetType = InputType.Gamepad;

            if (config.InputType != targetType)
            {
                throw new ArgumentException(string.Format("Given layout is not for {0}.", targetType.GetName()));
            }

            Keys = config.KeyIndices;
            Axes = config.AxisIndices;
        }

        internal void AddTmpButton(int func, int keyCode)
        {
            if (Keys[func] == (int)GPKeyCode.None)
            {
                if (m_tmpButtons == null)
                    m_tmpButtons = new List<int>();

                m_tmpButtons.Add(func);
                Keys[func] = keyCode;
            }
        }

        internal void RemoveTmpButton()
        {
            if (m_tmpButtons != null && m_tmpButtons.Count > 0)
            {
                for (int i = 0; i < m_tmpButtons.Count; i++)
                {
                    Keys[m_tmpButtons[i]] = (int)GPKeyCode.None;
                }

                m_tmpButtons.Clear();
            }
        }
    }
}
