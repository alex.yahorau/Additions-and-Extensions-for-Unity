﻿using Hargrim.Collections;
using Hargrim.Controls.ControlStuff;
using System;
using UnityEngine;

#pragma warning disable CS0169, CS0649
namespace Hargrim.Controls
{
    [CreateAssetMenu(menuName = "Input/Layout Config", fileName = "LayoutConfig")]
    public sealed class LayoutConfig : ScriptableObject
    {
        [SerializeField]
        private string m_keyEnumType;
        [SerializeField]
        private string m_axisEnumType;

        [SerializeField]
        internal InputType InputType;

        [SerializeField]
        internal int[] KeyIndices;
        [SerializeField]
        internal int[] AxisIndices;
        [SerializeField]
        internal KeyAxisDir[] AxisRel;

        private object m_layout;

        public InputType Type
        {
            get { return InputType; }
        }

        public ReadOnlyArray<int> GetKeys()
        {
            return new ReadOnlyArray<int>(KeyIndices);
        }

        public ReadOnlyArray<int> GetAxes()
        {
            return new ReadOnlyArray<int>(AxisIndices);
        }

        public ReadOnlyArray<KeyAxisDir> GetKeyAxes()
        {
            return new ReadOnlyArray<KeyAxisDir>(AxisRel);
        }

        internal KeyMouseBindLayout ToKeyMouseBindLayout()
        {
            if (m_layout == null)
                m_layout = new KeyMouseBindLayout(this);

            return m_layout as KeyMouseBindLayout;
        }

        internal GamepadBindLayout ToGamepadBindLayout()
        {
            if (m_layout == null)
                m_layout = new GamepadBindLayout(this);

            return m_layout as GamepadBindLayout;
        }
    }
}
