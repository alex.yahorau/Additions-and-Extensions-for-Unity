using System.Runtime.InteropServices;
using UnityEngine;

namespace Hargrim.Controls
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ScreenTouch
    {
        public int fingerId;
        public Vector2 position;
        public Vector2 prevPosition;
        public Vector2 deltaPosition;
        public float lastTime;
        public float deltaTime;
        public int tapCount;
        public TouchPhase phase;
    }

    public static class ScreenTouchExtensions
    {
        public static ScreenTouch ToEmulTouch(this Touch touch)
        {
            return new ScreenTouch()
            {
                position = touch.position,
                prevPosition = touch.position - touch.deltaPosition,
                deltaPosition = touch.deltaPosition,
                deltaTime = touch.deltaTime,
                lastTime = Time.time - touch.deltaTime,
                phase = touch.phase,
            };
        }

        public static ScreenTouch ToEmulTouch(this Touch touch, Vector2 prevPos)
        {
            return new ScreenTouch()
            {
                position = touch.position,
                prevPosition = prevPos,
                deltaPosition = touch.position - prevPos,
                deltaTime = touch.deltaTime,
                lastTime = Time.time - touch.deltaTime,
                phase = touch.phase,
            };
        }
    }

    public sealed class TouchInputEmulator
    {
        private ScreenTouch[] m_touches;
        private int m_touchCount;

        public ScreenTouch[] Touches
        {
            get { return m_touches; }
        }

        public int TouchCount
        {
            get { return m_touchCount; }
        }

        public TouchInputEmulator()
        {
            ScreenTouch touch = new ScreenTouch();
            touch.fingerId = 0;
            touch.position = Input.mousePosition;
            touch.prevPosition = Input.mousePosition;
            touch.deltaPosition = new Vector2(0f, 0f);
            touch.lastTime = Time.time;
            touch.deltaTime = 0f;
            touch.tapCount = 0;
            touch.phase = TouchPhase.Canceled;

            m_touches = new[] { touch };

            m_touchCount = 0;
        }

        public void ResetTouch()
        {
            m_touches[0].deltaTime = Time.time - m_touches[0].lastTime;
            m_touches[0].lastTime = Time.time;
            m_touches[0].phase = TouchPhase.Canceled;
            m_touchCount = 0;
        }

        public void Update()
        {
            Vector2 curMousePos = Input.mousePosition;

            switch (m_touches[0].phase)
            {
                case TouchPhase.Canceled:
                    if (Input.GetMouseButtonDown(0) == true)
                    {
                        m_touches[0].fingerId = 0;
                        m_touches[0].position = curMousePos;
                        m_touches[0].prevPosition = curMousePos;
                        m_touches[0].deltaPosition = new Vector2(0, 0);
                        m_touches[0].deltaTime = 0f;
                        m_touches[0].lastTime = Time.time;
                        m_touches[0].tapCount = 0;
                        m_touches[0].phase = TouchPhase.Began;
                        m_touchCount = 1;
                    }
                    break;

                case TouchPhase.Began:
                    m_touches[0].deltaTime = Time.time - m_touches[0].lastTime;
                    m_touches[0].lastTime = Time.time;
                    m_touches[0].phase = TouchPhase.Stationary;
                    break;

                case TouchPhase.Stationary:
                    if (m_touches[0].position != curMousePos)
                    {
                        m_touches[0].deltaPosition = curMousePos - m_touches[0].position;
                        m_touches[0].prevPosition = m_touches[0].position;
                        m_touches[0].position = curMousePos;
                        m_touches[0].deltaTime = Time.time - m_touches[0].lastTime;
                        m_touches[0].lastTime = Time.time;
                        m_touches[0].phase = TouchPhase.Moved;
                    }

                    if (Input.GetMouseButtonUp(0) == true)
                    {
                        m_touches[0].deltaTime = Time.time - m_touches[0].lastTime;
                        m_touches[0].lastTime = Time.time;
                        m_touches[0].phase = TouchPhase.Ended;
                    }
                    break;

                case TouchPhase.Moved:
                    if (m_touches[0].position != curMousePos)
                    {
                        m_touches[0].deltaPosition = curMousePos - m_touches[0].position;
                        m_touches[0].prevPosition = m_touches[0].position;
                        m_touches[0].position = curMousePos;
                        m_touches[0].deltaTime = Time.time - m_touches[0].lastTime;
                        m_touches[0].lastTime = Time.time;
                    }
                    else
                    {
                        m_touches[0].prevPosition = m_touches[0].position;
                        m_touches[0].deltaPosition = new Vector2(0, 0);
                        m_touches[0].deltaTime = Time.time - m_touches[0].lastTime;
                        m_touches[0].lastTime = Time.time;
                        m_touches[0].phase = TouchPhase.Stationary;
                    }

                    if (Input.GetMouseButtonUp(0) == true)
                    {
                        m_touches[0].deltaTime = Time.time - m_touches[0].lastTime;
                        m_touches[0].lastTime = Time.time;
                        m_touches[0].phase = TouchPhase.Ended;
                    }
                    break;

                case TouchPhase.Ended:
                    m_touches[0].deltaTime = Time.time - m_touches[0].lastTime;
                    m_touches[0].lastTime = Time.time;
                    m_touches[0].phase = TouchPhase.Canceled;
                    m_touchCount = 0;
                    break;
            }
        }
    }
}
