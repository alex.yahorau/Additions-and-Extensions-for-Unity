﻿using System;
using UnityEngine;

namespace Hargrim.Controls.ControlStuff
{
    public static class InputUnility
    {
        internal static KeyCode CreateKeyCode(KeyCode keyCodeRaw, int padNum)
        {
            Type type = typeof(KeyCode);

            if (keyCodeRaw < KeyCode.JoystickButton0)
            {
                return keyCodeRaw;
            }
            else
            {
                string name = Enum.GetName(type, keyCodeRaw);
                int index = name.IndexOf('B');
                string newName = string.Concat(name.Substring(0, index), padNum.ToString(), name.Substring(index));
                return (KeyCode)Enum.Parse(type, newName);
            }
        }

        public static string AxisName(int axNum, int padNum)
        {
            char pref = (char)('@' + padNum);

            string body = axNum < 10 ? "0" + axNum.ToString() : axNum.ToString();

            return string.Concat(pref.ToString(), ":", body);
        }

        internal static KeyCode[] GetRawKeyCodes(GamepadType type)
        {
            switch (type)
            {
                case GamepadType.XBoxWin:
                    return new[]
                    {
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.JoystickButton2,
                        KeyCode.JoystickButton1,
                        KeyCode.JoystickButton0,
                        KeyCode.JoystickButton3,
                        KeyCode.JoystickButton4,
                        KeyCode.JoystickButton5,
                        KeyCode.JoystickButton8,
                        KeyCode.JoystickButton9,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.JoystickButton6,
                        KeyCode.JoystickButton7
                    };

                case GamepadType.XBoxAndroid:
                    return new[]
                    {
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.JoystickButton2,
                        KeyCode.JoystickButton1,
                        KeyCode.JoystickButton0,
                        KeyCode.JoystickButton3,
                        KeyCode.LeftShift,
                        KeyCode.RightShift,
                        KeyCode.JoystickButton8,
                        KeyCode.JoystickButton9,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.Pause,
                        KeyCode.Return
                    };

                case GamepadType.DualShockWin:
                    return new[]
                    {
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.JoystickButton0,
                        KeyCode.JoystickButton2,
                        KeyCode.JoystickButton1,
                        KeyCode.JoystickButton3,
                        KeyCode.JoystickButton4,
                        KeyCode.JoystickButton5,
                        KeyCode.JoystickButton10,
                        KeyCode.JoystickButton11,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.JoystickButton8,
                        KeyCode.JoystickButton9
                    };

                case GamepadType.DualShockAndroid:
                    return new[]
                    {
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.JoystickButton0,
                        KeyCode.JoystickButton13,
                        KeyCode.JoystickButton1,
                        KeyCode.JoystickButton2,
                        KeyCode.JoystickButton3,
                        KeyCode.JoystickButton14,
                        KeyCode.Pause,
                        KeyCode.Return,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.LeftAlt,
                        KeyCode.RightAlt
                    };

                case GamepadType.GoogleAndroid:
                    return new[]
                    {
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.JoystickButton2,
                        KeyCode.JoystickButton1,
                        KeyCode.JoystickButton0,
                        KeyCode.JoystickButton3,
                        KeyCode.LeftShift,
                        KeyCode.RightShift,
                        KeyCode.JoystickButton8,
                        KeyCode.JoystickButton9,
                        KeyCode.None,
                        KeyCode.None,
                        KeyCode.Escape,
                        KeyCode.Return
                    };

                default: throw new UnsupportedValueException(type);
            }
        }

        internal static int[] GetRawAxisCodes(GamepadType type)
        {
            switch (type)
            {
                case GamepadType.XBoxWin:
                    return new[] { 6, 7, 1, 2, 4, 5, 9, 10 };

                case GamepadType.XBoxAndroid:
                    return new[] { 5, 6, 1, 2, 3, 4, 14, 15 };

                case GamepadType.DualShockWin:
                    return new[] { 7, 8, 1, 2, 3, 6, 4, 5 };

                case GamepadType.DualShockAndroid:
                    return new[] { 5, 6, 1, 2, 14, 15, 3, 4 };

                case GamepadType.GoogleAndroid:
                    return new[] { 5, 6, 1, 2, 3, 4, 13, 12 };

                default: throw new UnsupportedValueException(type);
            }
        }

        public static KeyCode GetPressedKey()
        {
            if (Input.anyKeyDown)
            {
                for (int i = 0; i < 500; i++)
                {
                    if (Input.GetKeyDown((KeyCode)i))
                    { return (KeyCode)i; }
                }
            }

            return KeyCode.None;
        }

        public static GPKeyCode GetPressedKey(GamepadType type)
        {
            if (Input.anyKeyDown)
            {
                KeyCode[] codes = GetRawKeyCodes(type);

                for (int i = 0; i < InputEnum.GPKeyCodeCount; i++)
                {
                    if (Input.GetKeyDown(codes[i]))
                    { return (GPKeyCode)i; }
                }
            }

            return GPKeyCode.None;
        }
    }
}
