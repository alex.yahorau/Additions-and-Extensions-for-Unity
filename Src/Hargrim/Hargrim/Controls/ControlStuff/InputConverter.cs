﻿using UnityEngine;
using System.Linq;

namespace Hargrim.Controls.ControlStuff
{
    internal class InputConverter
    {
        internal KeyCode[] KeyCodes;
        internal string[] AxisNames;

        internal InputConverter(GamepadType type, int padNum)
        {
            padNum++;

            KeyCode[] keyCodes = InputUnility.GetRawKeyCodes(type);
            KeyCodes = keyCodes.Select(itm => InputUnility.CreateKeyCode(itm, padNum)).ToArray();

            int[] axisCodes = InputUnility.GetRawAxisCodes(type);
            AxisNames = axisCodes.Select(itm => InputUnility.AxisName(itm, padNum)).ToArray();
        }
    }
}
