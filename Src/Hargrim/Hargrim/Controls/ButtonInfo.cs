﻿namespace Hargrim.Controls
{
    public struct ButtonInfo
    {
        internal int Function;
        internal int KeyCode;
    }
}
