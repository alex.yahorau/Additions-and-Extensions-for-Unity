﻿using Hargrim.Controls.ControlStuff;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Hargrim.Controls
{
    [Serializable]
    public sealed class KeyMouseBindLayout
    {
        [SerializeField]
        internal int[] Keys;
        [SerializeField]
        internal int[] Axes;
        [SerializeField]
        internal KeyAxisDir[] KeyAxes;

        private List<int> m_tmpButtons;

        public KeyMouseBindLayout(int[] keyIndices, int[] axisIndices, KeyAxisDir[] keyAxisAccord)
        {
            Keys = keyIndices;
            Axes = axisIndices;
            KeyAxes = keyAxisAccord;
        }

        internal KeyMouseBindLayout(LayoutConfig config)
        {
            InputType targetType = InputType.KeyMouse;

            if (config.InputType != targetType)
            {
                throw new ArgumentException(string.Format("Given layout is not for {0}.", targetType.GetName()));
            }

            Keys = config.KeyIndices;
            Axes = config.AxisIndices;
            KeyAxes = config.AxisRel;
        }

        internal void AddTmpButton(int func, int keyCode)
        {
            if (Keys[func] == (int)KeyCode.None)
            {
                if (m_tmpButtons == null)
                    m_tmpButtons = new List<int>();

                m_tmpButtons.Add(func);
                Keys[func] = keyCode;
            }
        }

        internal void RemoveTmpButton()
        {
            if (m_tmpButtons != null && m_tmpButtons.Count > 0)
            {
                for (int i = 0; i < m_tmpButtons.Count; i++)
                {
                    Keys[m_tmpButtons[i]] = (int)KeyCode.None;
                }

                m_tmpButtons.Clear();
            }
        }
    }
}
