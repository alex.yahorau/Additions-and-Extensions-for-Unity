﻿using System;
using System.Collections;
using UnityEngine;

namespace Hargrim
{
    /// <summary>
    /// Inherits from MonoBehaviour and provides several subsidiary methods.
    /// </summary>
    public class Script : MonoBehaviour
    {
        /// <summary>
        /// Creates and returns an instance of GameObject with Component of T type.
        /// </summary>
        /// <typeparam name="T">Type of component.</typeparam>
        /// <param name="isImmortal">Whether the created instance of GameObject should be destroyed.</param>
        public static T CreateInstance<T>(bool isImmortal = false) where T : Component
        {
            GameObject go = new GameObject(typeof(T).Name);
            if (isImmortal) { go.Immortalize(); }
            return go.AddComponent(typeof(T)) as T;
        }

        /// <summary>
        /// Creates and returns an instance of GameObject with Component of T type.
        /// </summary>
        /// <typeparam name="T">Type of component.</typeparam>
        /// <param name="isImmortal">Whether the created instance of GameObject should be destroyed.</param>
        public static T CreateInstance<T>(string gameObjectName, bool isImmortal = false) where T : Component
        {
            GameObject go = new GameObject(gameObjectName);
            if (isImmortal) { go.Immortalize(); }
            return go.AddComponent(typeof(T)) as T;
        }

        //--//

        /// <summary>
        /// Runs a referenced function after delay.
        /// </summary>
        public void RunDelayed(float time, Action run)
        {
            StartCoroutine(RunDelayedRoutine(time, run));
        }

        /// <summary>
        /// Runs a referenced function when <paramref name="condition"/> is true.
        /// </summary>
        public void RunByCondition(Func<bool> condition, Action run)
        {
            StartCoroutine(RunByConditionRoutine(condition, run));
        }

        /// <summary>
        /// Runs a referenced function on the next frame.
        /// </summary>
        public void RunNextFrame(Action run)
        {
            StartCoroutine(RunAfterFramesRoutine(1, run));
        }

        /// <summary>
        /// Runs a referenced function after specified frames count.
        /// </summary>
        public void RunAfterFrames(int frames, Action run)
        {
            StartCoroutine(RunAfterFramesRoutine(frames, run));
        }

        /// <summary>
        /// Runs a referenced function each frame while <paramref name="condition"/> is true.
        /// </summary>
        public void RunWhile(Func<bool> condition, Action run)
        {
            StartCoroutine(RunWhileRoutine(condition, run));
        }

        //--//

        internal static IEnumerator RunDelayedRoutine(float time, Action run)
        {
            while (time > 0f)
            {
                yield return null;
                time -= Time.deltaTime;
            }

            run();
        }

        internal static IEnumerator RunByConditionRoutine(Func<bool> condition, Action run)
        {
            while (!condition()) { yield return null; }

            run();
        }

        internal static IEnumerator RunAfterFramesRoutine(int frames, Action run)
        {
            while (frames > 0)
            {
                frames--;
                yield return null;
            }

            run();
        }

        internal static IEnumerator RunWhileRoutine(Func<bool> condition, Action run)
        {
            while (condition())
            {
                run();
                yield return null;
            }
        }
    }
}
