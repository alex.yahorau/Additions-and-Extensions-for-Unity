﻿using System;

namespace Hargrim.RNGenerators
{
    public class SimleTimeRNG : RNG
    {
        private readonly uint MULTIPLIER;

        private ulong m_seed;
        private ulong m_ticks;

        public SimleTimeRNG()
        {
            DateTime dt = DateTime.Now;

            MULTIPLIER = (uint)(111 + dt.Millisecond) % 1000;
            m_seed = (ulong)dt.Ticks;
        }

        public int Next(int minValue, int maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException(nameof(minValue), "minValue cannot be more than maxValue.");

            return (int)f_next(minValue, maxValue);
        }

        public void NextBytes(byte[] buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = (byte)Next(0, 256);
            }
        }

        public double NextDouble()
        {
            long rn = f_next(0, 1000000000000000);
            return rn * 0.000000000000001;
        }

        public float NextFloat(float minValue, float maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException(nameof(minValue), "minValue cannot be more than maxValue.");

            return (float)(NextDouble() * ((double)maxValue - minValue) + minValue);
        }

        private long f_next(long min, long max)
        {
            ulong size = (ulong)(max - min);
            ulong ticks = (ulong)DateTime.Now.Ticks;

            if (ticks > m_ticks)
                m_ticks = ticks;
            else
                m_ticks++;

            m_seed = (MULTIPLIER * m_seed + m_ticks) % size;

            return (long)m_seed + min;
        }
    }
}
