﻿using Hargrim.MathExt;
using System;

namespace Hargrim.RNGenerators.Stuff
{
    public class RNGDateTimeProvider : System.Security.Cryptography.RandomNumberGenerator
    {
        private readonly uint MULTIPLIER;

        private byte m_seed;
        private ulong m_ticks;

        public RNGDateTimeProvider()
        {
            MULTIPLIER = (uint)(111 + DateTime.Now.Millisecond) % 1000;
        }

        public RNGDateTimeProvider(int seed) : this()
        {
            m_seed = (byte)(seed.Abs() % 256);
        }

        public override void GetBytes(byte[] data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = f_getByte();
            }
        }

        public override void GetNonZeroBytes(byte[] data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = (byte)(f_getByte() | 1);
            }
        }

        private byte f_getByte()
        {
            ulong ticks = (ulong)DateTime.Now.Ticks;

            if (ticks > m_ticks)
                m_ticks = ticks;
            else
                m_ticks++;

            return m_seed = (byte)((MULTIPLIER * m_seed + m_ticks) % 256);
        }
    }
}
