﻿using static UnityEngine.Random;

namespace Hargrim.RNGenerators
{
    public sealed class UnityRNG : RNG
    {
        public UnityRNG() { }

        public UnityRNG(int seed)
        {
            InitState(seed);
        }

        public int Next(int minValue, int maxValue)
        {
            return Range(minValue, maxValue);
        }

        public float NextFloat(float minValue, float maxValue)
        {
            return Range(minValue, maxValue);
        }

        public void NextBytes(byte[] buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = (byte)Range(0, 256);
            }
        }

        public double NextDouble()
        {
            return NextFloat(0f, 1f - Rnd.MIN_STEP);
        }
    }
}
