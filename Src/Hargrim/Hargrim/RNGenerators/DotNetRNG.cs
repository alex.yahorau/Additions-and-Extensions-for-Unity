﻿using System;

namespace Hargrim.RNGenerators
{
    public sealed class DotNetRNG : Random, RNG
    {
        public DotNetRNG() { }

        public DotNetRNG(int seed) : base(seed) { }

        public float NextFloat(float minValue, float maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException(nameof(minValue), "minValue cannot be more than maxValue.");

            return (float)(Sample() * ((double)maxValue - minValue) + minValue);
        }
    }
}
