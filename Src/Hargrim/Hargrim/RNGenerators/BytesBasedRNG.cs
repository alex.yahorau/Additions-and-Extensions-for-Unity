﻿using System;
using Hargrim.RNGenerators.Stuff;
using System.Security.Cryptography;

namespace Hargrim.RNGenerators
{
    public class BytesBasedRNG : RNG
    {
        private RandomNumberGenerator m_rng;

        private byte[] m_bytes8;
        private byte[] m_bytes4;
        private byte[] m_bytes2;
        private byte[] m_bytes1;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="randomBytesProvider">For example System.Security.Cryptography.RNGCryptoServiceProvider or Hargrim.RNGenerators.Stuff.RNGDateTimeProvider.</param>
        public BytesBasedRNG(RandomNumberGenerator randomBytesProvider)
        {
            m_rng = randomBytesProvider;

            m_bytes8 = new byte[sizeof(ulong)];
            m_bytes4 = new byte[sizeof(uint)];
            m_bytes2 = new byte[sizeof(ushort)];
            m_bytes1 = new byte[sizeof(byte)];
        }

        ~BytesBasedRNG()
        {
            m_rng.Dispose();
        }

        public static BytesBasedRNG CreateCryptoRNG()
        {
            return new BytesBasedRNG(new RNGCryptoServiceProvider());
        }

        public static BytesBasedRNG CreateDateTimeRNG()
        {
            return new BytesBasedRNG(new RNGDateTimeProvider());
        }

        public int Next(int minValue, int maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException(nameof(minValue), "minValue cannot be more than maxValue.");

            long length = (long)maxValue - minValue;

            if (length < byte.MaxValue)
            {
                byte rn, len = (byte)length;
                m_rng.GetBytes(m_bytes1);
                f_convert(m_bytes1, out rn);
                return rn % len + minValue;
            }
            else if (length < ushort.MaxValue)
            {
                ushort rn, len = (ushort)length;
                m_rng.GetBytes(m_bytes2);
                f_convert(m_bytes2, out rn);
                return rn % len + minValue;
            }
            else if (length < uint.MaxValue)
            {
                uint rn, len = (uint)length;
                m_rng.GetBytes(m_bytes4);
                f_convert(m_bytes4, out rn);
                return (int)(rn % len + minValue);
            }
            else
            {
                ulong rn, len = (ulong)length;
                m_rng.GetBytes(m_bytes8);
                f_convert(m_bytes8, out rn);
                return (int)((long)(rn % len) + minValue);
            }
        }

        public float NextFloat(float minValue, float maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException(nameof(minValue), "minValue cannot be more than maxValue.");

            return (float)(NextDouble() * ((double)maxValue - minValue) + minValue);
        }

        public double NextDouble()
        {
            ulong rn;
            m_rng.GetBytes(m_bytes8);
            f_convert(m_bytes8, out rn);
            rn %= 1000000000000000;
            return rn * 0.000000000000001;
        }

        public void NextBytes(byte[] buffer)
        {
            m_rng.GetBytes(buffer);
        }

        // -- //

        private unsafe static void f_convert(byte[] data, out ulong value)
        {
            fixed (byte* ptr = data) { value = *(ulong*)ptr; }
        }

        private unsafe static void f_convert(byte[] data, out uint value)
        {
            fixed (byte* ptr = data) { value = *(uint*)ptr; }
        }

        private unsafe static void f_convert(byte[] data, out ushort value)
        {
            fixed (byte* ptr = data) { value = *(ushort*)ptr; }
        }

        private unsafe static void f_convert(byte[] data, out byte value)
        {
            fixed (byte* ptr = data) { value = *ptr; }
        }
    }
}
