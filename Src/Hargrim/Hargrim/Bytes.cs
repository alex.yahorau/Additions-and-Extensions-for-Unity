﻿using System;
using System.Collections;
using System.Collections.Generic;
using Hargrim.Collections;
using UnityEngine;

namespace Hargrim
{
    [Serializable]
    public unsafe struct Bytes : IEquatable<Bytes>, IEnumerable<byte>
    {
        public const int SIZE = sizeof(int);

        [SerializeField, HideInInspector]
        private int m_field;

        public int Size
        {
            get { return SIZE; }
        }

        internal string SerFieldName
        {
            get { return nameof(m_field); }
        }

        public byte this[int index]
        {
            get
            {
                f_throw(index);

                return f_get(m_field, index);
            }
            set
            {
                f_throw(index);

                int val = m_field;
                ((byte*)&val)[index] = value;
                m_field = val;
            }
        }

        public byte[] GetBytes()
        {
            return BitConverter.GetBytes((int)this);
        }

        // -- //        

        private static byte f_get(int field, int index)
        {
            return ((byte*)&field)[index];
        }

        private static void f_throw(int index)
        {
            if (index < 0 || index > SIZE - 1)
                throw new IndexOutOfRangeException("The index is out of range.");
        }

        // -- //

        public static int Convert(float value)
        {
            return *(int*)(&value);
        }

        public static float Convert(int value)
        {
            return *(float*)(&value);
        }

        // -- //

        #region regular stuff
        public override int GetHashCode()
        {
            return m_field.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is Bytes && this == (Bytes)obj;
        }

        public bool Equals(Bytes other)
        {
            return this == other;
        }

        public override string ToString()
        {
            byte b0 = f_get(m_field, 0);
            byte b1 = f_get(m_field, 1);
            byte b2 = f_get(m_field, 2);
            byte b3 = f_get(m_field, 3);

            return string.Format("({0}.{1}.{2}.{3})", b0, b1, b2, b3);
        }

        public IEnumerator<byte> GetEnumerator()
        {
            for (int i = 0; i < SIZE; i++)
            {
                yield return f_get(m_field, i);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        // Operators //

        public static bool operator ==(Bytes a, Bytes b)
        {
            return a.m_field == b.m_field;
        }

        public static bool operator !=(Bytes a, Bytes b)
        {
            return a.m_field != b.m_field;
        }

        // -- //

        public static explicit operator BitMask(Bytes bytes) { return *(BitMask*)(&bytes); }

        public static explicit operator Percent(Bytes bytes) { return *(Percent*)(&bytes); }

        public static explicit operator float(Bytes bytes) { return *(float*)(&bytes); }

        public static explicit operator int(Bytes bytes) { return bytes.m_field; }

        public static explicit operator uint(Bytes bytes) { return *(uint*)(&bytes); }

        public static explicit operator short(Bytes bytes) { return (short)bytes.m_field; }

        public static explicit operator ushort(Bytes bytes) { return (ushort)bytes.m_field; }

        public static explicit operator bool(Bytes bytes) { return (uint)bytes.m_field != 0; }


        // -- //

        public static implicit operator Bytes(BitMask val) { return *(Bytes*)(&val); }

        public static implicit operator Bytes(Percent val) { return *(Bytes*)(&val); }

        public static implicit operator Bytes(float val) { return *(Bytes*)(&val); }

        public static implicit operator Bytes(int val) { return *(Bytes*)(&val); }

        public static implicit operator Bytes(uint val) { return *(Bytes*)(&val); }

        public static implicit operator Bytes(short val) { return new Bytes { m_field = val }; }

        public static implicit operator Bytes(ushort val) { return new Bytes { m_field = val }; }

        public static implicit operator Bytes(bool val) { return new Bytes { m_field = val ? -1 : 0 }; }
    }
}
