using UnityEngine;

#pragma warning disable CS0649
namespace Hargrim.Scripts
{
    [RequireComponent(typeof(Renderer))]
    public class RenderSorter : Script
    {
        [SerializeField, HideInInspector]
        private Renderer _renderer;
        [SerializeField, SortingLayerID]
        private int _sortingLayer;
        [SerializeField]
        private int _sortingOrder;

        private void Awake()
        {
            if (_renderer == null)
            {
                _renderer = GetComponent<Renderer>();
                if (_renderer == null) { return; }
            }

            _renderer.sortingLayerID = _sortingLayer;
            _renderer.sortingOrder = _sortingOrder;
        }
    }
}
