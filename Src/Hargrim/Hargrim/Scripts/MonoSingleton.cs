﻿namespace Hargrim.Scripts
{
    /// <summary>
    /// Represents implementation of MonoBehaviour singleton with lazy initialization.
    /// </summary>
    public abstract class MonoSingleton<T> : Script where T : MonoSingleton<T>
    {
        private static T m_inst;

        /// <summary>
        /// Static instance of SingleScript.
        /// </summary>
        public static T I
        {
            get { return m_inst ?? (m_inst = CreateInstance<T>()); }
        }

        /// <summary>
        /// Returns true if the instance is not null.
        /// </summary>
        public static bool Exists
        {
            get { return m_inst != null; }
        }

        private void OnDestroy()
        {
            m_inst = null;
            Dispose();
        }

        /// <summary>
        /// Use it instead of OnDestroy.
        /// </summary>
        protected abstract void Dispose();
    }
}
