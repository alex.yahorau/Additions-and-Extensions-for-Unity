﻿using System;
using System.Collections;
using UnityEngine;
using Hargrim.MathExt;

namespace Hargrim.Scripts
{
    public sealed class MonoTimer : Script, Timer
    {
        #region IEnumerator impelementation
        private class Enumerator : IEnumerator
        {
            public float WaitTime = 1f;
            public Action Callback;
            public bool IsRunning;
            public float CurTime = 1f;

            private bool m_isFirst = true;

            //IEnumerator//

            public object Current
            {
                get { return null; }
            }

            public bool MoveNext()
            {
                if (CurTime < WaitTime)
                {
                    if (m_isFirst)
                        m_isFirst = false;
                    else
                        CurTime += Time.deltaTime;
                }
                else
                {
                    IsRunning = false;
                    if (Callback != null)
                        Callback();
                }

                return IsRunning;
            }

            public void Reset()
            {
                CurTime = 0f;
                m_isFirst = true;
            }
        }
        #endregion

        private Enumerator mEnumerator = new Enumerator();

        public bool IsRunning
        {
            get { return mEnumerator.IsRunning; }
        }

        public float TargetTime
        {
            get { return mEnumerator.WaitTime; }
        }

        public float CurTime
        {
            get { return mEnumerator.CurTime.CutAfter(mEnumerator.WaitTime); }
        }

        public float Progress
        {
            get { return CurTime / TargetTime; }
        }

        public void Prolong(float addTime)
        {
            mEnumerator.WaitTime += addTime;
        }

        public void InitCallback(Action callback)
        {
            mEnumerator.Callback = callback;
        }

        public void StartCountdown(float time, Action callback)
        {
            InitCallback(callback);

            fStart(time);
        }

        public void StartCountdown(float time)
        {
            fStart(time);
        }

        public void StopCountdown()
        {
            StopAllCoroutines();
            mEnumerator.IsRunning = false;
        }

        // -- //

        private void fStart(float time)
        {
            mEnumerator.Reset();

            mEnumerator.WaitTime = time;
            mEnumerator.IsRunning = true;

            StartCoroutine(mEnumerator);
        }
    }
}
