﻿using Hargrim.MathExt;
using System;

namespace Hargrim.NumericEntities
{
    [Serializable]
    public struct AccumEntity : AbstractAccumEntity<float>
    {
        [UnityEngine.SerializeField]
        private float m_got;
        [UnityEngine.SerializeField]
        private float m_spent;

        public float Value
        {
            get { return m_got - m_spent; }
        }

        public bool IsEmpty
        {
            get { return m_got.Nearly(m_spent); }
        }

        public float Got
        {
            get { return m_got; }
        }

        public float Spent
        {
            get { return m_spent; }
        }

        public AccumEntity(float got, float spent)
        {
            if (spent > got)
                throw new ArgumentOutOfRangeException(nameof(spent), "spent value cannot be more than got value.");

            m_got = got;
            m_spent = spent;
        }

        public void Add(float addValue)
        {
            if (addValue < 0f)
                throw new ArgumentOutOfRangeException(nameof(addValue), "value cannot be less than zero.");

            m_got += addValue;
        }

        public bool Spend(float spendValue)
        {
            if (spendValue < 0f)
                throw new ArgumentOutOfRangeException(nameof(spendValue), "value cannot be less than zero.");

            if (spendValue <= Value)
            {
                m_spent += spendValue;
                return true;
            }

            return false;
        }
    }
}
