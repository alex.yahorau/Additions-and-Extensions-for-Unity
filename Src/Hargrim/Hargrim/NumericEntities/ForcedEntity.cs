﻿using Hargrim.MathExt;
using System;

namespace Hargrim.NumericEntities
{
    [Serializable]
    public struct ForcedEntity : AbstractForcedEntity<float>
    {
        [UnityEngine.SerializeField]
        private float m_threshold;
        [UnityEngine.SerializeField]
        private float m_expander;

        public float CurValue
        {
            get { return m_expander.Clamp(0f, m_threshold); }
        }

        public float Threshold
        {
            get { return m_threshold; }
        }

        public bool LimitReached
        {
            get { return m_expander >= m_threshold; }
        }

        public bool Forced
        {
            get { return m_expander > float.Epsilon; }
        }

        public float Ratio
        {
            get { return m_expander / m_threshold; }
        }

        public ForcedEntity(float threshold)
        {
            if (threshold < 0f)
                throw new ArgumentOutOfRangeException(nameof(threshold), "threshold cannot be less than zero.");

            m_threshold = threshold;
            m_expander = 0f;
        }

        public void Force(float value)
        {
            if (value < 0f)
                throw new ArgumentOutOfRangeException(nameof(value), "value cannot be less than zero.");

            m_expander += value.Clamp(0f, m_threshold - m_expander);
        }

        public void Restore(float value)
        {
            if (value < 0f)
                throw new ArgumentOutOfRangeException(nameof(value), "value cannot be less than zero.");

            m_expander -= value.Clamp(0f, m_expander);
        }

        public void RestoreFull()
        {
            m_expander = 0f;
        }

        public void Resize(float value, ResizeType resizeType = ResizeType.NewValue)
        {
            if (value < 0f)
                throw new ArgumentOutOfRangeException(nameof(value), "value cannot be less than zero.");

            switch (resizeType)
            {
                case ResizeType.NewValue:
                    m_threshold = value;
                    m_expander = m_expander.Clamp(0f, m_threshold);
                    break;

                case ResizeType.Increase:
                    m_threshold += value;
                    break;

                case ResizeType.Decrease:
                    m_threshold -= value.Clamp(0f, m_threshold);
                    m_expander = m_expander.Clamp(0f, m_threshold);
                    break;

                default:
                    throw new UnsupportedValueException(resizeType);
            }
        }
    }
}
