﻿using Hargrim.MathExt;
using System;

namespace Hargrim.NumericEntities
{
    [Serializable]
    public struct SpendingEntity : AbstractSpendingEntity<float>
    {
        [UnityEngine.SerializeField]
        private float m_capacity;
        [UnityEngine.SerializeField]
        private float m_reducer;

        public float Capacity
        {
            get { return m_capacity; }
        }

        public float CurValue
        {
            get { return (m_capacity - m_reducer).CutBefore(0f); }
        }

        public float ReducingExcess
        {
            get { return (m_capacity - m_reducer).CutAfter(0f).Abs(); }
        }

        public float Ratio
        {
            get { return CurValue / m_capacity; }
        }

        public bool IsFull
        {
            get { return m_reducer.Nearly(0f); }
        }

        public bool IsEmpty
        {
            get { return m_reducer >= m_capacity; }
        }

        public SpendingEntity(float capacity)
        {
            if (capacity < 0f)
                throw new ArgumentOutOfRangeException(nameof(capacity), "capacity cannot be less than zero.");

            m_capacity = capacity;
            m_reducer = 0f;
        }

        public void Reduce(float value)
        {
            if (value < 0f)
                throw new ArgumentOutOfRangeException(nameof(value), "value cannot be less than zero.");

            m_reducer += value;
        }

        public void Restore(float value)
        {
            if (value < 0f)
                throw new ArgumentOutOfRangeException(nameof(value), "value cannot be less than zero.");

            var min = Math.Min(m_reducer, m_capacity);
            m_reducer = min - value.CutAfter(min);
        }

        public void RestoreFull()
        {
            m_reducer = 0f;
        }

        public void Resize(float value, ResizeType resizeType = ResizeType.NewValue)
        {
            if (value < 0f)
                throw new ArgumentOutOfRangeException(nameof(value), "value cannot be less than zero.");

            switch (resizeType)
            {
                case ResizeType.NewValue:
                    m_capacity = value;
                    m_reducer = m_reducer.Clamp(0f, m_capacity);
                    break;

                case ResizeType.Increase:
                    m_capacity += value;
                    break;

                case ResizeType.Decrease:
                    m_capacity -= value.Clamp(0f, m_capacity);
                    m_reducer = m_reducer.Clamp(0f, m_capacity);
                    break;

                default:
                    throw new UnsupportedValueException(resizeType);
            }
        }
    }
}
