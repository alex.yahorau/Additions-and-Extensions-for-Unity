﻿namespace Hargrim.NumericEntities
{
    public enum ResizeType : byte { NewValue, Increase, Decrease }
}
