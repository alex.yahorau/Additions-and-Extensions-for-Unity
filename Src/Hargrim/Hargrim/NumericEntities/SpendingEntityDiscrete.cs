﻿using Hargrim.MathExt;
using System;

namespace Hargrim.NumericEntities
{
    [Serializable]
    public struct SpendingEntityDiscrete : AbstractSpendingEntity<int>
    {
        [UnityEngine.SerializeField]
        private int m_capacity;
        [UnityEngine.SerializeField]
        private int m_reducer;

        public int Capacity
        {
            get { return m_capacity; }
        }

        public int CurValue
        {
            get { return (m_capacity - m_reducer).CutBefore(0); }
        }

        public int ReducingExcess
        {
            get { return (m_capacity - m_reducer).CutAfter(0).Abs(); }
        }

        public bool IsEmpty
        {
            get { return m_reducer >= m_capacity; }
        }

        public bool IsFull
        {
            get { return m_reducer == 0; }
        }

        public float Ratio
        {
            get { return (float)CurValue / m_capacity; }
        }

        public SpendingEntityDiscrete(int capacity)
        {
            if (capacity < 0)
                throw new ArgumentOutOfRangeException(nameof(capacity), "capacity cannot be less than zero.");

            m_capacity = capacity;
            m_reducer = 0;
        }

        public void Reduce(int value)
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(nameof(value), "value cannot be less than zero.");

            m_reducer += value;
        }

        public void Restore(int value)
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(nameof(value), "value cannot be less than zero.");

            var min = Math.Min(m_reducer, m_capacity);
            m_reducer = min - value.CutAfter(min);
        }

        public void RestoreFull()
        {
            m_reducer = 0;
        }

        public void Resize(int value, ResizeType resizeType = ResizeType.NewValue)
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(nameof(value), "value cannot be less than zero.");

            switch (resizeType)
            {
                case ResizeType.NewValue:
                    m_capacity = value;
                    m_reducer = m_reducer.Clamp(0, m_capacity);
                    break;

                case ResizeType.Increase:
                    m_capacity += value;
                    break;

                case ResizeType.Decrease:
                    m_capacity -= value.Clamp(0, m_capacity);
                    m_reducer = m_reducer.Clamp(0, m_capacity);
                    break;

                default:
                    throw new UnsupportedValueException(resizeType);
            }
        }
    }
}
