﻿using UnityEngine;

namespace Hargrim
{
    public abstract class UIScript : Script
    {
        public RectTransform rectTransform
        {
            get { return transform as RectTransform; }
        }
    }
}
