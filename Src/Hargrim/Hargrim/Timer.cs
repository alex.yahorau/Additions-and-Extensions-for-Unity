﻿using System;

namespace Hargrim
{
    public interface Timer
    {
        bool IsRunning { get; }
        float TargetTime { get; }
        float CurTime { get; }
        float Progress { get; }

        void InitCallback(Action callback);

        void StartCountdown(float time);
        void StartCountdown(float time, Action callback);

        void Prolong(float time);

        void StopCountdown();
    }
}
