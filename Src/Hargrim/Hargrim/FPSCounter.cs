﻿using System;

namespace Hargrim
{
    /// <summary>
    /// Counts FPS.
    /// </summary>
    public sealed class FPSCounter
    {
        private float m_refreshTime;

        private int m_frameCounter;
        private float m_timeCounter;

        private float m_curFPS;

        public float RefreshTime
        {
            get { return m_refreshTime; }
        }

        public float FrameRate
        {
            get { return m_curFPS; }
        }

        public FPSCounter(float refreshTime = 0.5f)
        {
            if (refreshTime <= 0f)
            {
                throw new ArgumentOutOfRangeException(nameof(refreshTime), "refresh time must be more than zero.");
            }

            m_refreshTime = refreshTime;
        }

        public void ChangeRefreshRate(float value)
        {
            m_refreshTime = value;
        }

        /// <summary>
        /// Needs to be called in each frame.
        /// </summary>
        /// <param name="deltaTime">Time passed from previous frame.</param>
        public void Update(float deltaTime)
        {
            m_frameCounter++;
            m_timeCounter += deltaTime;

            if (m_timeCounter >= m_refreshTime)
            {
                m_curFPS = m_frameCounter / m_refreshTime;

                m_frameCounter = 0;
                m_timeCounter = 0f;
            }
        }
    }
}