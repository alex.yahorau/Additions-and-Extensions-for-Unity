using System;
using UnityEngine;
using System.Collections.Generic;
using Hargrim.Sound.SoundProviderStuff;
using Hargrim.Collections;
using Hargrim.MathExt;

namespace Hargrim.Sound
{
    public static class SoundProvider
    {
        private struct Key
        {
            public string Name;
            public object Sender;

            public Key(object sender, string name)
            {
                Name = name;
                Sender = sender;
            }
        }

        private class Data
        {
            public bool Locked;

            public bool IsMuted;
            public float SndVolume = 1f;

            public ClipLoader Loader = new DefaultClipLoader("Sounds/");
            public Dictionary<Key, SndObject> KeyedSounds = new Dictionary<Key, SndObject>();
            public HashSet<SndObject> FreeSounds = new HashSet<SndObject>();
            public ObjectPool<SndObject> Pool = new ObjectPool<SndObject>(f_createSoundObject);
            public Dictionary<string, SPreset> Preset = new Dictionary<string, SPreset>();
        }

        private static readonly SPreset DEF_SET;
        private static Data m_inst;

        public static bool Muted
        {
            get { return m_inst.IsMuted; }
        }

        public static float Volume
        {
            get { return m_inst.SndVolume; }
        }

        static SoundProvider()
        {
            m_inst = new Data();
            DEF_SET = new SPreset { Volume = 1f, Pitch = 1f, MinDist = 1f, MaxDist = 500f };
        }

        public static void OverrideLoader(ClipLoader loader)
        {
            m_inst.Loader = loader;
        }

        public static void InitPreset(SoundsPreset preset)
        {
            m_inst.Preset = preset.CreateDict();
        }

        public static void SetVolume(float value)
        {
            m_inst.SndVolume = value.Saturate();

            foreach (var kvp in m_inst.KeyedSounds) { kvp.Value.UpdVolume(); }
            foreach (var snd in m_inst.FreeSounds) { snd.UpdVolume(); }
        }

        public static void PlaySound(string soundName)
        {
            SPreset set;

            if (!m_inst.Preset.TryGetValue(soundName, out set))
            {
                set = DEF_SET;
            }

            SndObject snd = m_inst.Pool.Get();
            snd.Play(null, m_inst.Loader.LoadClip(soundName), set);
            m_inst.FreeSounds.Add(snd);
        }

        public static void PlaySound(object sender, string soundName, bool breakCurrent = false)
        {
            SPreset set;

            if (!m_inst.Preset.TryGetValue(soundName, out set))
            {
                set = DEF_SET;
            }

            SndObject snd;
            Key key = new Key(sender, soundName);

            if (!m_inst.KeyedSounds.TryGetValue(key, out snd))
            {
                snd = m_inst.KeyedSounds.AddNGet(key, m_inst.Pool.Get());
                snd.Play(sender, m_inst.Loader.LoadClip(soundName), set);
            }
            else if (breakCurrent && !snd.IsLooped)
            {
                snd.Restart(set);
            }
        }

        public static void PlaySound3D(string soundName, Vector3 pos)
        {
            SPreset set;

            if (!m_inst.Preset.TryGetValue(soundName, out set))
            {
                set = DEF_SET;
            }

            SndObject snd = m_inst.Pool.Get();
            snd.Play3D(null, m_inst.Loader.LoadClip(soundName), set, pos);
            m_inst.FreeSounds.Add(snd);
        }

        public static void PlaySound3D(Transform sender, string soundName, bool breakCurrent = false)
        {
            SPreset set;

            if (!m_inst.Preset.TryGetValue(soundName, out set))
            {
                set = DEF_SET;
            }

            SndObject snd;
            Key key = new Key(sender, soundName);

            if (!m_inst.KeyedSounds.TryGetValue(key, out snd))
            {
                snd = m_inst.KeyedSounds.AddNGet(key, m_inst.Pool.Get());
                snd.Play3D(sender, m_inst.Loader.LoadClip(soundName), set);
            }
            else if (breakCurrent && !snd.IsLooped)
            {
                snd.Restart(set);
            }
        }

        public static void StopSound(object sender, string soundName)
        {
            SndObject snd;

            if (m_inst.KeyedSounds.TryGetValue(new Key(sender, soundName), out snd))
            {
                snd.Stop();
            }
        }

        public static void StopSoundFading(object sender, string soundName, float intensity = 0.5f)
        {
            SndObject snd;

            if (m_inst.KeyedSounds.TryGetValue(new Key(sender, soundName), out snd))
            {
                snd.StopFading(intensity);
            }
        }

        public static void StopAllSounds()
        {
            m_inst.Locked = true;

            foreach (var kvp in m_inst.KeyedSounds) { kvp.Value.Stop(); }
            m_inst.KeyedSounds.Clear();
            foreach (var snd in m_inst.FreeSounds) { snd.Stop(); }
            m_inst.FreeSounds.Clear();

            m_inst.Locked = false;
        }

        public static void MuteAllSounds(bool value)
        {
            if (m_inst.IsMuted != value)
            {
                m_inst.IsMuted = value;

                foreach (var kvp in m_inst.KeyedSounds) { kvp.Value.Mute(value); }
                foreach (var snd in m_inst.FreeSounds) { snd.Mute(value); }
            }
        }

        internal static void ReleaseSound(SndObject snd)
        {
            RemoveSound(snd);

            m_inst.Pool.Release(snd);
        }

        internal static void RemoveSound(SndObject snd)
        {
            if (!m_inst.Locked)
            {
                if (snd.Sender == null) { m_inst.FreeSounds.Remove(snd); }
                else { m_inst.KeyedSounds.Remove(new Key(snd.Sender, snd.ClipName)); }
            }
        }

        //--//

        private static SndObject f_createSoundObject()
        {
            return Script.CreateInstance<SndObject>(true);
        }
    }
}
