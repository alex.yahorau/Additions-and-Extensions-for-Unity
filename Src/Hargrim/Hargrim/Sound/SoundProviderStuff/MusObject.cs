﻿using UnityEngine;
using System.Collections;
using Hargrim.Collections;

namespace Hargrim.Sound.SoundProviderStuff
{
    internal class MusObject : Script, Poolable, SoundObjectInfo
    {
        private AudioSource m_audioSource;
        private float m_volume;

        public string ClipName
        {
            get { return m_audioSource.clip.name; }
        }

        AudioSource SoundObjectInfo.AudioSource
        {
            get { return m_audioSource; }
        }

        ///////////////
        //Unity Funcs//
        ///////////////

        private void Awake()
        {
            m_audioSource = gameObject.AddComponent<AudioSource>();
            m_audioSource.playOnAwake = false;
        }

        private void Update()
        {
            if (m_audioSource.isPlaying) { return; }

            Stop();
        }

        ////////////////
        //Public Funcs//
        ////////////////        

        internal void Play(AudioClip clip, MPreset set)
        {
            m_audioSource.clip = clip;
            m_audioSource.loop = set.Looped;
            m_audioSource.time = set.Time;
            m_audioSource.pitch = set.Pitch;

            if (set.Rising)
            {
                m_volume = 0f;
                StartCoroutine(Rise(set));
            }
            else
            {
                m_volume = set.Volume;
            }

            UpdVolume();
            m_audioSource.PlayDelayed(set.Delay);
        }

        public void Stop()
        {
            StopAllCoroutines();
            m_audioSource.Stop();
            MusicProvider.ReleaseMusic(this);
        }

        internal void StopFading(float intensity)
        {
            StopAllCoroutines();
            StartCoroutine(FadeAndStop(intensity));
        }

        internal void Mute(bool value)
        {
            m_audioSource.mute = value;
        }

        internal void UpdVolume()
        {
            m_audioSource.volume = m_volume * MusicProvider.Volume;
        }

        #region IPoolable
        void Poolable.Reinit()
        {
            gameObject.SetActive(true);
        }

        void Poolable.CleanUp()
        {
            gameObject.SetActive(false);
            m_audioSource.clip = null;
        }
        #endregion

        ////////////
        //Routines//
        ////////////

        IEnumerator Rise(MPreset set)
        {
            float indicator = 0f;

            while (indicator < set.Delay)
            {
                yield return null;
                indicator += Time.deltaTime;
            }

            indicator = 0f;

            while (indicator < 1f)
            {
                indicator += set.Intensity * Time.deltaTime;
                m_volume = Mathf.Lerp(0f, set.Volume, indicator);
                UpdVolume();

                yield return null;
            }

            m_audioSource.volume = set.Volume;
        }

        IEnumerator FadeAndStop(float intensity)
        {
            float ratio = 0f;
            float startVal = m_volume;

            while (ratio < 1f)
            {
                ratio += intensity * Time.deltaTime;
                m_volume = Mathf.Lerp(startVal, 0f, ratio);
                UpdVolume();

                yield return null;
            }

            m_audioSource.Stop();
            MusicProvider.ReleaseMusic(this);
        }
    }
}
