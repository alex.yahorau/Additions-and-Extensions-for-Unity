﻿using UnityEngine;

namespace Hargrim.Sound.SoundProviderStuff
{
    internal interface SoundObjectInfo
    {
        string ClipName { get; }
        AudioSource AudioSource { get; }
        void Stop();
    }
}
