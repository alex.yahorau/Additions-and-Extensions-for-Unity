﻿using UnityEngine;

namespace Hargrim.Sound.SoundProviderStuff
{
    public interface ClipLoader
    {
        AudioClip LoadClip(string name);
    }
}
