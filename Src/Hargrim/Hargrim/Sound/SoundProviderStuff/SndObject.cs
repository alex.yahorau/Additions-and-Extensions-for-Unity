using UnityEngine;
using System.Collections;
using Hargrim.Collections;

namespace Hargrim.Sound.SoundProviderStuff
{
    internal class SndObject : Script, Poolable, SoundObjectInfo
    {
        internal object Sender;

        private AudioSource m_audioSource;
        private float m_volume;

        public string ClipName
        {
            get { return m_audioSource.clip.name; }
        }

        internal bool IsLooped
        {
            get { return m_audioSource.loop; }
        }

        AudioSource SoundObjectInfo.AudioSource
        {
            get { return m_audioSource; }
        }

        ///////////////
        //Unity Funcs//
        ///////////////

        private void Awake()
        {
            m_audioSource = gameObject.AddComponent<AudioSource>();
            m_audioSource.playOnAwake = false;
            m_audioSource.dopplerLevel = 0f;
        }

        private void Update()
        {
            if (m_audioSource.isPlaying) { return; }

            Stop();
        }

        private void OnDestroy()
        {
            SoundProvider.RemoveSound(this);
        }

        ////////////////
        //Public Funcs//
        ////////////////

        internal void Play(object sender, AudioClip clip, SPreset set)
        {
            Sender = sender;
            m_audioSource.clip = clip;
            f_play(set);
        }

        internal void Play3D(Transform sender, AudioClip clip, SPreset set, Vector3 pos = default(Vector3))
        {
            Sender = sender;
            m_audioSource.clip = clip;
            m_audioSource.minDistance = set.MinDist;
            m_audioSource.maxDistance = set.MaxDist;
            m_audioSource.spatialBlend = 1f;

            if (sender == null) { transform.position = pos; }
            else { transform.SetParent(sender, Vector3.zero); }

            f_play(set);
        }

        internal void Restart(SPreset set)
        {
            StopAllCoroutines();
            m_audioSource.Stop();
            f_play(set);
        }

        public void Stop()
        {
            StopAllCoroutines();
            m_audioSource.Stop();
            SoundProvider.ReleaseSound(this);
        }

        internal void StopFading(float intensity)
        {
            StopAllCoroutines();
            StartCoroutine(FadeAndStop(intensity));
        }

        internal void Mute(bool value)
        {
            m_audioSource.mute = value;
        }

        internal void UpdVolume()
        {
            m_audioSource.volume = m_volume * SoundProvider.Volume;
        }

        #region IPoolable
        void Poolable.Reinit()
        {
            gameObject.SetActive(true);
        }

        void Poolable.CleanUp()
        {
            if (Sender != null && Sender is Transform) { transform.Free(); }
            gameObject.SetActive(false);
            m_audioSource.clip = null;
            m_audioSource.spatialBlend = 0f;
            Sender = null;
        }
        #endregion

        //////////////
        //Inner fncs//
        //////////////

        private void f_play(SPreset set)
        {
            m_volume = set.Volume;
            m_audioSource.loop = Sender == null ? false : set.Looped;
            m_audioSource.pitch = set.Pitch;
            m_audioSource.mute = SoundProvider.Muted;
            UpdVolume();
            m_audioSource.Play();
        }

        ////////////
        //Routines//
        ////////////

        IEnumerator FadeAndStop(float intensity)
        {
            float ratio = 0f;
            float startVal = m_volume;

            while (ratio < 1f)
            {
                ratio += intensity * Time.deltaTime;
                m_volume = Mathf.Lerp(startVal, 0f, ratio);
                UpdVolume();
                yield return null;
            }

            m_audioSource.Stop();
            SoundProvider.ReleaseSound(this);
        }
    }
}
