﻿using System;
using UnityEngine;
using System.Collections.Generic;

#pragma warning disable CS0649
namespace Hargrim.Sound.SoundProviderStuff
{
    [Serializable]
    internal class MPreset
    {
        [SerializeField]
        internal bool Looped;
        [SerializeField]
        internal bool Rising;
        [SerializeField]
        internal float Volume;
        [SerializeField]
        internal float Pitch;
        [SerializeField]
        internal float Delay;
        [SerializeField]
        internal float Time;
        [SerializeField]
        internal float Intensity;
    }

    [CreateAssetMenu(menuName = "Sound/Music Preset", fileName = "MusicPreset")]
    public sealed class MusicPreset : ScriptableObject
    {
        [Serializable]
        private struct Node
        {
            [SerializeField]
            internal AudioClip Clip;
            [SerializeField]
            internal MPreset Stats;
        }

        [SerializeField]
        private Node[] m_nodes;

        internal Dictionary<string, MPreset> CreateDict()
        {
            var dict = new Dictionary<string, MPreset>(m_nodes.Length);

            for (int i = 0; i < m_nodes.Length; i++)
            {
                if (m_nodes[i].Clip != null)
                {
                    dict.Add(m_nodes[i].Clip.name, m_nodes[i].Stats);
                }
            }

            return dict;
        }
    }
}
