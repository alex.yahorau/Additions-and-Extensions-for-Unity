using System;
using System.Collections.Generic;
using Hargrim.Sound.SoundProviderStuff;
using Hargrim.Collections;
using Hargrim.MathExt;

namespace Hargrim.Sound
{
    public static class MusicProvider
    {
        private class Data
        {
            public bool Locked;

            public bool IsMuted;
            public float MusVolume = 1f;

            public ClipLoader Loader = new DefaultClipLoader("Music/");
            public ObjectPool<MusObject> Pool = new ObjectPool<MusObject>(f_createMusicObject);
            public Dictionary<string, MusObject> Music = new Dictionary<string, MusObject>();
            public Dictionary<string, MPreset> Preset = new Dictionary<string, MPreset>();
        }

        private static readonly MPreset DEF_SET;
        private static Data m_inst;

        public static bool Muted
        {
            get { return m_inst.IsMuted; }
        }

        public static float Volume
        {
            get { return m_inst.MusVolume; }
        }

        static MusicProvider()
        {
            m_inst = new Data();
            DEF_SET = new MPreset { Volume = 1f, Pitch = 1f, Looped = true, Intensity = 0.5f };
        }

        public static void OverrideLoader(ClipLoader loader)
        {
            m_inst.Loader = loader;
        }

        public static void InitPreset(MusicPreset preset)
        {
            m_inst.Preset = preset.CreateDict();
        }

        public static void SetVolume(float value)
        {
            m_inst.MusVolume = value.Saturate();

            foreach (var kvp in m_inst.Music)
            {
                kvp.Value.UpdVolume();
            }
        }

        public static void PlayMusic(string musicName)
        {
            if (!m_inst.Music.ContainsKey(musicName))
            {
                MPreset set;

                if (!m_inst.Preset.TryGetValue(musicName, out set))
                {
                    set = DEF_SET;
                }

                MusObject mus = m_inst.Music.AddNGet(musicName, m_inst.Pool.Get());
                mus.Play(m_inst.Loader.LoadClip(musicName), set);
            }
        }

        public static void StopMusic(string musicName)
        {
            MusObject mus;

            if (m_inst.Music.TryGetValue(musicName, out mus))
            {
                mus.Stop();
            }
        }

        public static void StopMusicFading(string musicName, float intensity = 0.5f)
        {
            MusObject mus;

            if (m_inst.Music.TryGetValue(musicName, out mus))
            {
                mus.StopFading(intensity);
            }
        }

        public static void StopAllMusic()
        {
            m_inst.Locked = true;

            foreach (var kvp in m_inst.Music)
            {
                kvp.Value.Stop();
            }

            m_inst.Music.Clear();

            m_inst.Locked = false;
        }

        public static void StopAllMusicFading(float intensity = 0.5f)
        {
            foreach (var kvp in m_inst.Music)
            {
                kvp.Value.StopFading(intensity);
            }
        }

        public static void MuteAllMusic(bool value)
        {
            if (m_inst.IsMuted != value)
            {
                m_inst.IsMuted = value;

                foreach (var kvp in m_inst.Music) { kvp.Value.Mute(value); }
            }
        }

        internal static void ReleaseMusic(MusObject mus)
        {
            if (!m_inst.Locked)
            {
                m_inst.Music.Remove(mus.ClipName);
            }

            m_inst.Pool.Release(mus);
        }

        //--//

        private static MusObject f_createMusicObject()
        {
            return Script.CreateInstance<MusObject>(true);
        }
    }
}
