﻿using Hargrim.MathExt;
using System.Runtime.InteropServices;
using UnityEngine;
using static System.Math;

namespace Hargrim
{
    public struct Arc2
    {
        private float m_angle;
        private float m_sin;
        private float m_cos;

        public float Speed;
        public float Gravity;

        public float Angle
        {
            get { return m_angle; }
            set
            {
                m_angle = value;
                float angle = value.ToRadians();
                m_sin = (float)Sin(angle);
                m_cos = (float)Cos(angle);
            }
        }

        public Arc2(float angle, float startSpeed, float gravity)
        {
            m_angle = angle;
            angle = angle.ToRadians();
            m_sin = (float)Sin(angle);
            m_cos = (float)Cos(angle);

            Speed = startSpeed;
            Gravity = gravity;
        }

        public Arc2(Vector2 dir, float startSpeed, float gravity)
        {
            float angle = Vector3.Angle(Vector2.right, dir);
            m_angle = dir.y >= 0f ? angle : -angle;
            angle = m_angle.ToRadians();
            m_sin = (float)Sin(angle);
            m_cos = (float)Cos(angle);

            Speed = startSpeed;
            Gravity = gravity;
        }

        public Vector2 Evaluate(float time)
        {
            float x = Speed * time * m_cos;
            float y = (Speed * m_sin - Gravity * time * 0.5f) * time;
            return new Vector2(x, y);
        }

        internal Vector2 Evaluate(float time, float wind)
        {
            float x = (Speed * m_cos + wind * time * 0.5f) * time;
            float y = (Speed * m_sin - Gravity * time * 0.5f) * time;
            return new Vector2(x, y);
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Arc2Windy
    {
        private Arc2 m_arc;
        private float Wind;

        public float Angle
        {
            get { return m_arc.Angle; }
            set { m_arc.Angle = value; }
        }

        public float Speed
        {
            get { return m_arc.Speed; }
            set { m_arc.Speed = value; }
        }

        public float Gravity
        {
            get { return m_arc.Gravity; }
            set { m_arc.Gravity = value; }
        }

        public Arc2Windy(float angle, float startSpeed, float gravity, float wind = 0f)
        {
            Wind = wind;
            m_arc = new Arc2(angle, startSpeed, gravity);
        }

        public Arc2Windy(Vector2 dir, float startSpeed, float gravity, float wind = 0f)
        {
            Wind = wind;
            m_arc = new Arc2(dir, startSpeed, gravity);
        }

        public Vector2 Evaluate(float time)
        {
            return m_arc.Evaluate(time, Wind);
        }
    }
}
