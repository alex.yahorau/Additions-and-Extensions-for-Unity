using Debug = UnityEngine.Debug;

using System;
using System.Diagnostics;
using UnityEngine;

namespace Hargrim
{
    /// <summary>
    /// UnityEngine.Debug wrapper which is created mostly to hide the logger property from substitution in C# text editor.
    /// </summary>
    public static class Msg
    {
        /// <summary>
        /// Event for the custom console or terminal implementation.
        /// </summary>
        public static event Action<LogType, object> Log_Event;

        /// <summary>
		/// Logs message to the Unity Console.
		/// </summary>
        public static void Log(object msg)
        {
            Debug.Log(msg);
            f_event(LogType.Log, msg);
        }

        /// <summary>
        /// A variant of the Log function that logs a warning message to the console.
        /// </summary>
        /// <param name="msg"></param>
        public static void Warning(object msg)
        {
            Debug.LogWarning(msg);
            f_event(LogType.Warning, msg);
        }

        /// <summary>
        /// A variant of the Log function that logs an error message to the console.
        /// </summary>
        public static void Error(object msg)
        {
            Debug.LogError(msg);
            f_event(LogType.Error, msg);
        }

        /// <summary>
        /// Asserts a condition and logs an error message to the Unity console on failure.
        /// </summary>
        [Conditional("UNITY_ASSERTIONS")]
        public static void Assert(bool condition)
        {
            if (!condition)
            {
                string msg = "Assertion failed";
                Debug.LogWarning(msg);
                f_event(LogType.Assert, msg);
            }
        }

        /// <summary>
        /// Asserts a condition and logs an error message to the Unity console on failure.
        /// </summary>
        [Conditional("UNITY_ASSERTIONS")]
        public static void Assert(bool condition, object message)
        {
            if (!condition)
            {
                Debug.LogWarning(message);
                f_event(LogType.Assert, message);
            }
        }

        /// <summary>
        /// Checks a condition and logs a message to the Unity console on success.
        /// </summary>
        [Conditional("UNITY_ASSERTIONS")]
        public static void Check(bool condition)
        {
            if (condition)
            {
                string msg = "Check passed";
                Debug.Log(msg);
                f_event(LogType.Log, msg);
            }
        }

        /// <summary>
        /// Checks a condition and logs a message to the Unity console on success.
        /// </summary>
        [Conditional("UNITY_ASSERTIONS")]
        public static void Check(bool condition, object message)
        {
            if (condition)
            {
                Debug.Log(message);
                f_event(LogType.Log, message);
            }
        }

        /// <summary>
        /// Draws a line between specified start and end points.
        /// </summary>
        public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0f)
        {
            Debug.DrawLine(start, end, color, duration, true);
        }

        /// <summary>
        /// Draws a line from start to start + dir in world coordinates.
        /// </summary>
        public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration = 0f)
        {
            Debug.DrawLine(start, start + dir, color, duration, true);
        }

        //--//

        private static void f_event(LogType logType, object message)
        {
            if (Log_Event != null) { Log_Event(logType, message); }
        }
    }
}
