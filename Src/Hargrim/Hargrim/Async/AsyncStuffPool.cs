﻿using Hargrim.Collections;
using Hargrim.IDGenerating;
using UnityEngine.SceneManagement;

namespace Hargrim.Async
{
    internal static class AsyncStuffPool
    {
        private class Data
        {
            private readonly string RUNNER_NAME = "Task";

            public ObjectPool<RoutineExecutor> RunnersPool;
            public UintIDGenerator IDs;

            public Data()
            {
                IDs = new UintIDGenerator();
                RunnersPool = new ObjectPool<RoutineExecutor>(f_create);

                SceneManager.sceneUnloaded += SceneUnloadedHandler;
            }

            private RoutineExecutor f_create()
            {
                return Script.CreateInstance<RoutineExecutor>(RUNNER_NAME);
            }

            private void SceneUnloadedHandler(Scene scene)
            {
                RunnersPool.Clear();
            }
        }

        private static Data m_inst;

        static AsyncStuffPool()
        {
            m_inst = new Data();
        }

        internal static uint GetNewId()
        {
            return m_inst.IDs.GetNewId();
        }

        internal static RoutineExecutor GetExecutor()
        {
            return m_inst.RunnersPool.Get();
        }

        internal static void Return(RoutineExecutor runner)
        {
            m_inst.RunnersPool.Release(runner);
        }
    }
}
