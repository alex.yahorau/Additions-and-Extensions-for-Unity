﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace Hargrim.Async
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct TaskInfo
    {
        private readonly uint m_ID;
        internal RoutineExecutor Runner;

        /// <summary>
        /// Provides task ID.
        /// </summary>
        public uint TaskId
        {
            get { return m_ID; }
        }

        /// <summary>
        /// Returns true if the task is alive. The task is alive while it runs.
        /// </summary>
        public bool IsAlive
        {
            get { return f_isAlive(); }
        }

        public bool IsPaused
        {
            get { return f_isAlive() && Runner.IsPaused; }
        }

        internal TaskInfo(RoutineExecutor runner)
        {
            Runner = runner;
            m_ID = runner.ID;
        }

        /// <summary>
        /// Pauses the task.
        /// </summary>
        public void Pause()
        {
            if (f_isAlive()) { Runner.Pause(); }
        }

        /// <summary>
        /// Resumes paused task.
        /// </summary>
        public void Resume()
        {
            if (f_isAlive()) { Runner.Resume(); }
        }

        /// <summary>
        /// Stops the task and marks it as non-alive.
        /// </summary>
        public void Stop()
        {
            if (f_isAlive()) { Runner.Stop(); }
        }

        /// <summary>
        /// Adds new task to the queue. It is allowed if the current task is alive.
        /// </summary>
        public void ContinueWith(IEnumerator routine)
        {
            if (f_isAlive())
            {
                Runner.Add(routine);
                Runner.StartRunning();
            }
            else
            {
                throw new InvalidOperationException("Task is not alive.");
            }
        }

        // - - //

        private bool f_isAlive()
        {
            return Runner != null && Runner.ID == m_ID;
        }
    }
}
