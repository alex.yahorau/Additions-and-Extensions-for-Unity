﻿using Hargrim.MathExt;
using Hargrim.RNGenerators;
using System;

namespace Hargrim
{
    /// <summary>
    /// Class for generating random data.
    /// </summary>
    public static class Rnd
    {
        /// <summary>
        /// Description of nonlinear random.
        /// </summary>
        public enum Dependence : byte { Square, Circle }

        internal const float MIN_STEP = 0.0000001f;

        private static RNG m_rnd;

        static Rnd()
        {
            m_rnd = new UnityRNG();
        }

        public static void OverrideRandomizer(RNG randomizer)
        {
            m_rnd = randomizer;
        }

        /// <summary>
        /// Returns true with chance from 0f to 1f.
        /// </summary>
        public static bool Chance(float chance)
        {
            return chance > (float)m_rnd.NextDouble() - MIN_STEP;
        }

        public static bool Chance(Percent chance)
        {
            return Chance(chance.ToRatio());
        }

        public static int Chance(float[] chances)
        {
            float rnd = (float)m_rnd.NextDouble() - MIN_STEP;
            float sum = 0f;

            for (int i = 0; i < chances.Length; i++)
            {
                if (rnd < chances[i] + sum)
                    return i;

                sum += chances[i];
            }

            return -1;
        }

        /// <summary>
        /// Returns a random integer number between min [inclusive] and max [exclusive].
        /// </summary>
        public static int Random(int min, int max)
        {
            return m_rnd.Next(min, max);
        }

        /// <summary>
        /// Returns a random integer number between zero [inclusive] and max [exclusive].
        /// </summary>
        public static int Random(int max)
        {
            return m_rnd.Next(0, max);
        }

        /// <summary>
        /// Returns a random float number between min [inclusive] and max [inclusive].
        /// </summary>
        public static float Random(float min, float max)
        {
            return m_rnd.NextFloat(min, max);
        }

        /// <summary>
        /// Returns a random float number between zero [inclusive] and max [inclusive].
        /// </summary>
        public static float Random(float max)
        {
            return m_rnd.NextFloat(0f, max);
        }

        /// <summary>
        /// Returns a random integer number between min [inclusive] and max [exclusive] and which is satisfies the specified condition.
        /// </summary>
        public static int Random(int min, int max, Func<int, bool> condition)
        {
            int value;
            do { value = m_rnd.Next(min, max); } while (!condition(value));
            return value;
        }

        /// <summary>
        /// Returns a random float number between min [inclusive] and max [inclusive] and which is satisfies the specified condition.
        /// </summary>
        public static float Random(float min, float max, Func<float, bool> condition)
        {
            float value;
            do { value = m_rnd.NextFloat(min, max); } while (!condition(value));
            return value;
        }

        /// <summary>
        /// Returns a random float number between -range [inclusive] and range [inclusive].
        /// </summary>
        public static float Range(float range)
        {
            return m_rnd.NextFloat(-range, range);
        }

        /// <summary>
        /// Returns a random integer number between min [inclusive] and max [exclusive] and which is not equal to the specified value.
        /// </summary>
        public static int Random(int min, int max, int exclusiveValue)
        {
            int value;
            do { value = m_rnd.Next(min, max); } while (value == exclusiveValue);
            return value;
        }

        /// <summary>
        /// Returns a random float number between min [inclusive] and max [inclusive]. Min values have more chance than max ones.
        /// </summary>
        public static float NonLinear(float min, float max, Dependence type)
        {
            float range = max - min;

            switch (type)
            {
                case Dependence.Square:
                    return (1f - m_rnd.NextFloat(0f, 1f)).Pow(2) * range + min;

                case Dependence.Circle:
                    float x = m_rnd.NextFloat(0f, range);
                    return max - (x * (2f * range - x)).Sqrt();

                default:
                    throw new UnsupportedValueException(type);
            }
        }

        /// <summary>
        /// Returns a random even integer number between min [inclusive] and max [exclusive].
        /// </summary>
        public static int RandomEven(int min, int max)
        {
            return m_rnd.Next(min, max) & -2;
        }

        /// <summary>
        /// Returns a random odd integer number between min [inclusive] and max [exclusive].
        /// </summary>
        public static int RandomOdd(int min, int max)
        {
            return m_rnd.Next(min, max - 1) | 1;
        }

        /// <summary>
        /// Returns a random string of the specified length.
        /// </summary>
        /// <param name="oneByteValue">If true each char will be initialized by a value in one-byte range. Otherwise the value will be in two-bytes range.</param>
        public static string RandomString(int length, bool oneByteValue = true)
        {
            int maxSize = oneByteValue ? 256 : 65536;
            char[] chars = new char[length];
            for (int i = 0; i < length; i++)
            {
                chars[i] = (char)m_rnd.Next(0, maxSize);
            }
            return new string(chars);
        }

        /// <summary>
        /// Returns a random byte array of the specified length.
        /// </summary>
        public static void RandomByteArray(byte[] buffer)
        {
            m_rnd.NextBytes(buffer);
        }
    }
}
