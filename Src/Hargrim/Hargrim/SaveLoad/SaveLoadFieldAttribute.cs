﻿using System;
using System.Reflection;
using Hargrim.SaveLoad.SaveProviderStuff;

namespace Hargrim.SaveLoad
{
    /// <summary>
    /// Mark non-static fields which you want to save and load.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class SaveLoadFieldAttribute : Attribute
    {
        private string m_defValString;
        internal Bytes DefValSimple;
        internal string Key;

        internal FieldInfo Field;

        internal string DefValString
        {
            get { return m_defValString ?? string.Empty; }
        }

        public SaveLoadFieldAttribute() { }

        public SaveLoadFieldAttribute(string key)
        {
            Key = key;
        }

        public SaveLoadFieldAttribute(string key, string defValue)
        {
            Key = key;
            m_defValString = defValue;
        }

        public SaveLoadFieldAttribute(string key, int defValue)
        {
            Key = key;
            DefValSimple = defValue;
        }

        public SaveLoadFieldAttribute(string key, float defValue)
        {
            Key = key;
            DefValSimple = defValue;
        }

        public SaveLoadFieldAttribute(string key, bool defValue)
        {
            Key = key;
            DefValSimple = defValue;
        }
    }
}
