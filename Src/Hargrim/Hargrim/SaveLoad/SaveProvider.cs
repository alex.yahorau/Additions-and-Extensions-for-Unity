﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Hargrim.Async;
using Hargrim.SaveLoad.SaveProviderStuff;

namespace Hargrim.SaveLoad
{
    /// <summary>
    /// Keeps, saves and loads game data.
    /// </summary>
    public static class SaveProvider
    {
        private class Data
        {
            public readonly BindingFlags MASK = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;

            public Saver InnerSaver = new PlayerPrefsSaver();
            public Serializer Serializer = new JsonSerializer();

            public Dictionary<object, List<SaveLoadFieldAttribute>> Members = new Dictionary<object, List<SaveLoadFieldAttribute>>();
        }

        private static Data m_inst;

        public static Saver Saver
        {
            get { return m_inst.InnerSaver; }
        }

        static SaveProvider()
        {
            m_inst = new Data();
        }

        ////////////////
        //Public funcs//
        ////////////////

        /// <summary>
        /// Overrides the default saver which saves and loads objects data. The default saver is UnityEngine.PlayerPrefs wrapper.
        /// </summary>
        public static void OverrideSaver(Saver saver)
        {
            m_inst.InnerSaver = saver;
        }

        /// <summary>
        /// Overrides the default serializer which is used for saving custom value type fields. The default serializer uses UnityEngine.JsonUtility.
        /// </summary>
        public static void OverrideSerializer(Serializer serializer)
        {
            m_inst.Serializer = serializer;
        }

        /// <summary>
        /// Registers an object of which fields should be saved and load.
        /// </summary>
        /// <param name="initFields">If true the registered object fields will be initialized from saved data.</param>
        public static void RegMember(object client, bool initFields = true)
        {
            RegMember(client, null, initFields);
        }

        /// <summary>
        /// Registers an object of which fields should be saved and load.
        /// </summary>
        /// <param name="keyPrefix">Specific prefix for keys of the fields.</param>
        /// <param name="initFields">If true the registered object fields will be initialized from saved data.</param>
        public static void RegMember(object client, string keyPrefix, bool initFields = true)
        {
            Type t = client.GetType();
            FieldInfo[] fields = t.GetFields(m_inst.MASK);

            List<SaveLoadFieldAttribute> list = null;

            for (int i = 0; i < fields.Length; i++)
            {
                SaveLoadFieldAttribute a = fields[i].GetCustomAttribute<SaveLoadFieldAttribute>();

                if (a != null)
                {
                    if (list == null) { list = new List<SaveLoadFieldAttribute>(); }

                    a.Field = fields[i];

                    if (a.Key == null) { a.Key = a.Field.Name; }

                    if (keyPrefix != null) { a.Key = string.Concat(keyPrefix, "_", a.Key); }

                    list.Add(a);

                    if (initFields) { a.Field.SetValue(client, f_get(a)); }
                }
            }

            if (list != null) { m_inst.Members.Add(client, list); }
        }

        /// <summary>
        /// Unregisters the registered object.
        /// </summary>        
        /// <param name="deleteSaves">If true the saved data of the object will be deleted.</param>
        public static void UnregMember(object client, bool deleteSaves = false)
        {
            List<SaveLoadFieldAttribute> list;

            if (deleteSaves && m_inst.Members.TryGetValue(client, out list))
            {
                for (int i = 0; i < list.Count; i++)
                {
                    m_inst.InnerSaver.Delete(list[i].Key);
                }
            }

            m_inst.Members.Remove(client);
        }

        /// <summary>
        /// Collects data from marked by SaveLoadFieldAttribute fields of all registered objects and sets it as key-value data through the saver.
        /// </summary>
        public static void Collect()
        {
            foreach (var kvp in m_inst.Members)
            {
                List<SaveLoadFieldAttribute> aList = kvp.Value;
                for (int i = 0; i < aList.Count; i++)
                    f_set(aList[i].Key, aList[i].Field.GetValue(kvp.Key));
            }
        }

        public static TaskInfo CollectAsync(bool fieldPerFrame = true)
        {
            return Tasks.StartAsync(CollectRoutine(fieldPerFrame));
        }

        /// <summary>
        /// Saves collected data (calls PlayerPrefs.Save() in case of saver based on UnityEngine.PlayerPrefs).
        /// </summary>
        public static void ApplyAll()
        {
            m_inst.InnerSaver.ApplyAll();
        }

        /// <summary>
        /// Deletes all saved data.
        /// </summary>
        public static void DeleteAll()
        {
            m_inst.InnerSaver.DeleteAll();
        }

        ///////////////
        //Inner funcs//
        ///////////////

        private static void f_set(string key, object value)
        {
            if (value is int)
            {
                m_inst.InnerSaver.Set(key, (int)value);
            }
            else if (value is float)
            {
                m_inst.InnerSaver.Set(key, (float)value);
            }
            else if (value is bool)
            {
                m_inst.InnerSaver.Set(key, (bool)value);
            }
            else if (value is string)
            {
                m_inst.InnerSaver.Set(key, (string)value);
            }
            else
            {
                m_inst.InnerSaver.Set(key, m_inst.Serializer.Serialize(value));
            }
        }

        private static object f_get(SaveLoadFieldAttribute a)
        {
            Type type = a.Field.FieldType;

            if (type == typeof(int))
            {
                return m_inst.InnerSaver.Get(a.Key, (int)a.DefValSimple);
            }
            else if (type == typeof(float))
            {
                return m_inst.InnerSaver.Get(a.Key, (float)a.DefValSimple);
            }
            else if (type == typeof(bool))
            {
                return m_inst.InnerSaver.Get(a.Key, (bool)a.DefValSimple);
            }
            else if (type == typeof(string))
            {
                return m_inst.InnerSaver.Get(a.Key, a.DefValString);
            }
            else
            {
                string serStr = m_inst.InnerSaver.Get(a.Key, string.Empty);

                if (serStr.HasAnyData())
                {
                    return m_inst.Serializer.Deserialize(serStr, type);
                }

                return Activator.CreateInstance(type);
            }
        }

        ////////////
        //Routines//
        ////////////

        static IEnumerator CollectRoutine(bool fieldPerFrame)
        {
            foreach (var kvp in m_inst.Members)
            {
                List<SaveLoadFieldAttribute> aList = kvp.Value;

                for (int i = 0; i < aList.Count; i++)
                {
                    f_set(aList[i].Key, aList[i].Field.GetValue(kvp.Key));

                    if (fieldPerFrame) yield return null;
                }

                if (!fieldPerFrame) yield return null;
            }
        }
    }
}
