﻿using Hargrim.MathExt;
using System;
using UnityEngine;

namespace Hargrim.SaveLoad.SaveProviderStuff
{
    /// <summary>
    /// Saves and loads data through UnityEngine.PlayerPrefs.
    /// </summary>
    public sealed class PlayerPrefsSaver : Saver
    {
        public void ApplyAll()
        {
            PlayerPrefs.Save();
        }

        public void DeleteAll()
        {
            PlayerPrefs.DeleteAll();
        }

        //--//

        public bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }

        public void Delete(string key)
        {
            PlayerPrefs.DeleteKey(key);
        }

        //--//

        public int Get(string key, int defVal)
        {
            return PlayerPrefs.GetInt(key, defVal);
        }

        public float Get(string key, float defVal)
        {
            return PlayerPrefs.GetFloat(key, defVal);
        }

        public bool Get(string key, bool defVal)
        {
            return PlayerPrefs.GetInt(key, defVal.ToInt()).ToBool();
        }

        public string Get(string key, string defVal)
        {
            return PlayerPrefs.GetString(key, defVal);
        }

        //--//

        public void Set(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }

        public void Set(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }

        public void Set(string key, bool value)
        {
            PlayerPrefs.SetInt(key, value.ToInt());
        }

        public void Set(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }
    }
}
