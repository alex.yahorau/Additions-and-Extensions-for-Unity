﻿namespace Hargrim.SaveLoad.SaveProviderStuff
{
    public interface Serializer
    {
        string Serialize(object toSerialize);
        object Deserialize(string toDeserialize, System.Type type);
    }
}
