﻿namespace Hargrim.SaveLoad.SaveProviderStuff
{
    public interface Saver
    {
        int Get(string key, int defVal);
        float Get(string key, float defVal);
        bool Get(string key, bool defVal);
        string Get(string key, string defVal);

        void Set(string key, int value);
        void Set(string key, float value);
        void Set(string key, bool value);
        void Set(string key, string value);

        void Delete(string key);
        bool HasKey(string key);

        void ApplyAll();
        void DeleteAll();
    }
}
