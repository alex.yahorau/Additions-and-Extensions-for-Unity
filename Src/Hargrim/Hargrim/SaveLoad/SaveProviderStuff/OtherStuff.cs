﻿using System;
using System.Runtime.InteropServices;

namespace Hargrim.SaveLoad.SaveProviderStuff
{
#if DOT_NET_3_5
    internal static class FieldInfoExtension
    {
        public static T GetCustomAttribute<T>(this System.Reflection.FieldInfo element) where T : Attribute
        {
            var array = element.GetCustomAttributes(typeof(T), true);
            return array.Length > 0 ? array[0] as T : null;
        }
    }
#endif
}
