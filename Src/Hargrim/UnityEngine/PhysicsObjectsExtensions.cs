﻿using Hargrim.Collections;

namespace UnityEngine
{
    public static class PhysicsObjectsExtensions
    {
        /// <summary>
        /// Returns true if any object was hit. Otherwise returns false.
        /// </summary>
        public static bool Hit(this RaycastHit hit)
        {
            return hit.transform != null;
        }

        /// <summary>
        /// Calls GetComponent() on the game object that was hit.
        /// </summary>
        public static T GetComponent<T>(this RaycastHit hit)
        {
            return hit.transform.GetComponent<T>();
        }

        /// <summary>
        /// Returns the layer in which the game object that was hit is.
        /// </summary>
        public static int GetLayer(this RaycastHit hit)
        {
            return hit.transform.gameObject.layer;
        }

        /// <summary>
        /// Returns true if the layer of the hit game object equal to <paramref name="comparedLayer"/>.
        /// </summary>
        public static bool CompareLayer(this RaycastHit hit, int comparedLayer)
        {
            return hit.transform.gameObject.layer == comparedLayer;
        }

        public static bool CompareLayer(this RaycastHit hit, BitMask mask)
        {
            return mask.ContainsFlag(hit.transform.gameObject.layer);
        }

        public static bool CompareLayer(this RaycastHit hit, LayerMask mask)
        {
            return mask.HasLayer(hit.transform.gameObject.layer);
        }

        /// <summary>
        /// Calls GetComponent() on the game object that was hit.
        /// </summary>
        public static T GetComponent<T>(this RaycastHit2D hit)
        {
            return hit.transform.GetComponent<T>();
        }

        /// <summary>
        /// Returns the layer in which the game object that was hit is.
        /// </summary>
        public static int GetLayer(this RaycastHit2D hit)
        {
            return hit.transform.gameObject.layer;
        }

        /// <summary>
        /// Returns true if the layer of the hit game object equal to <paramref name="comparedLayer"/>.
        /// </summary>
        public static bool CompareLayer(this RaycastHit2D hit, int comparedLayer)
        {
            return hit.transform.gameObject.layer == comparedLayer;
        }

        public static bool CompareLayer(this RaycastHit2D hit, BitMask mask)
        {
            return mask.ContainsFlag(hit.transform.gameObject.layer);
        }

        public static bool CompareLayer(this RaycastHit2D hit, LayerMask mask)
        {
            return mask.HasLayer(hit.transform.gameObject.layer);
        }

        /// <summary>
        /// Returns the layer in which the game object that was hit is.
        /// </summary>
        public static int GetLayer(this Collision collision)
        {
            return collision.gameObject.layer;
        }

        /// <summary>
        /// Returns true if the layer of the hit game object equal to <paramref name="comparedLayer"/>.
        /// </summary>
        public static bool CompareLayer(this Collision collision, int comparedLayer)
        {
            return collision.gameObject.layer == comparedLayer;
        }

        public static bool CompareLayer(this Collision collision, BitMask mask)
        {
            return mask.ContainsFlag(collision.gameObject.layer);
        }

        public static bool CompareLayer(this Collision collision, LayerMask mask)
        {
            return mask.HasLayer(collision.gameObject.layer);
        }
    }
}
