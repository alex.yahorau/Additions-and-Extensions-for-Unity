﻿using Hargrim.MathExt;
using System;

namespace UnityEngine
{
    public static class UnityStructExtensions
    {
        public static Vector3 AlterX(this Vector3 value, float x = 0f)
        {
            value.x = x;
            return value;
        }

        public static Vector3 AlterY(this Vector3 value, float y = 0f)
        {
            value.y = y;
            return value;
        }

        public static Vector3 AlterZ(this Vector3 value, float z = 0f)
        {
            value.z = z;
            return value;
        }

        //--//

        /// <summary>
        /// Creates vector2 based on X and Y of vector3 value.
        /// </summary>
        public static Vector2 XY(this Vector3 value)
        {
            return new Vector2(value.x, value.y);
        }

        /// <summary>
        /// Creates vector2 based on X and Z of vector3 value.
        /// </summary>
        public static Vector2 XZ(this Vector3 value)
        {
            return new Vector2(value.x, value.z);
        }

        /// <summary>
        /// Creates vector2 based on Y and Z of vector3 value.
        /// </summary>
        public static Vector2 YZ(this Vector3 value)
        {
            return new Vector2(value.z, value.y);
        }

        //--//

        /// <summary>
        /// Converts vector2 params to vector3 X and Y with custom Z
        /// </summary>
        public static Vector3 To_XYz(this Vector2 value, float z = 0f)
        {
            return new Vector3(value.x, value.y, z);
        }

        /// <summary>
        /// Converts vector2 params to vector3 X and Z with custom Y
        /// </summary>
        public static Vector3 To_XyZ(this Vector2 value, float y = 0f)
        {
            return new Vector3(value.x, y, value.y);
        }

        /// <summary>
        /// Converts vector2 params to vector3 Y and Z with custom X
        /// </summary>
        public static Vector3 To_xYZ(this Vector2 value, float x = 0f)
        {
            return new Vector3(x, value.y, value.x);
        }

        //--//

#if DOT_NET_4_6

        public static Vector3Int AlterX(this Vector3Int value, int x = 0)
        {
            value.x = x;
            return value;
        }

        public static Vector3Int AlterY(this Vector3Int value, int y = 0)
        {
            value.y = y;
            return value;
        }

        public static Vector3Int AlterZ(this Vector3Int value, int z = 0)
        {
            value.z = z;
            return value;
        }

        //--//

        /// <summary>
        /// Creates vector2 based on X and Y of vector3 value.
        /// </summary>
        public static Vector2Int XY(this Vector3Int value)
        {
            return new Vector2Int(value.x, value.y);
        }

        /// <summary>
        /// Creates vector2 based on X and Z of vector3 value.
        /// </summary>
        public static Vector2Int XZ(this Vector3Int value)
        {
            return new Vector2Int(value.x, value.z);
        }

        /// <summary>
        /// Creates vector2 based on Y and Z of vector3 value.
        /// </summary>
        public static Vector2Int YZ(this Vector3Int value)
        {
            return new Vector2Int(value.z, value.y);
        }

        //--//

        /// <summary>
        /// Converts vector2 params to vector3 X and Y with custom Z
        /// </summary>
        public static Vector3Int To_XYz(this Vector2Int value, int z = 0)
        {
            return new Vector3Int(value.x, value.y, z);
        }

        /// <summary>
        /// Converts vector2 params to vector3 X and Z with custom Y
        /// </summary>
        public static Vector3Int To_XyZ(this Vector2Int value, int y = 0)
        {
            return new Vector3Int(value.x, y, value.y);
        }

        /// <summary>
        /// Converts vector2 params to vector3 Y and Z with custom X
        /// </summary>
        public static Vector3Int To_xYZ(this Vector2Int value, int x = 0)
        {
            return new Vector3Int(x, value.y, value.x);
        }

        /// <summary>
        /// Is position in bounds.
        /// </summary>
        /// <param name="bounds">Checked bounds</param>
        public static bool InBounds(this Vector2Int pos, RectInt bounds)
        {
            return bounds.Contains(pos);
        }

        /// <summary>
        /// Is position in bounds.
        /// </summary>
        /// <param name="bounds">Checked bounds</param>
        public static bool InBounds(this Vector3Int pos, RectInt bounds)
        {
            return bounds.Contains(pos.XY());
        }

        /// <summary>
        /// Returns vector2 value clamped between values represented by the bounds.
        /// </summary>
        public static Vector2Int GetClamped(this Vector2Int value, RectInt bounds)
        {
            return new Vector2Int
            {
                x = value.x.Clamp(bounds.xMin, bounds.xMax),
                y = value.y.Clamp(bounds.yMin, bounds.yMax)
            };
        }
#endif
        //--//

        /// <summary>
        /// Returns vector2 rotated in XY-plane.
        /// </summary>
        /// <param name="angle">Rotation angle in degrees.</param>
        public static Vector2 GetRotated(this Vector2 value, float angle)
        {
            angle = angle.ToRadians();
            float sin = (float)Math.Sin(angle);
            float cos = (float)Math.Cos(angle);

            return new Vector2(value.x * cos - value.y * sin, value.x * sin + value.y * cos);
        }

        /// <summary>
        /// Returns vector3 rotated in space.
        /// </summary>
        /// <param name="euler">Euler engles for rotation in degrees.</param>
        public static Vector3 GetRotated(this Vector3 value, Vector3 euler)
        {
            return Quaternion.Euler(euler) * value;
        }

        /// <summary>
        /// Returns vector3 rotated around specified axis.
        /// </summary>
        /// <param name="angle">Rotation angle in degrees.</param>
        public static Vector3 GetRotated(this Vector3 value, Vector3 axis, float angle)
        {
            angle = angle.ToRadians();
            return GetRotated(value, axis, (float)Math.Sin(angle), (float)Math.Cos(angle));
        }

        /// <summary>
        /// Returns vector3 rotated around specified axis.
        /// </summary>
        /// <param name="sin">Sin of the rotation angle.</param>
        /// <param name="cos">Cos of the rotation angle.</param>
        public static Vector3 GetRotated(this Vector3 value, Vector3 axis, float sin, float cos)
        {
            float oneMinusCos = 1f - cos;
            float oneMinusCosByXY = oneMinusCos * axis.x * axis.y;
            float oneMinusCosByYZ = oneMinusCos * axis.y * axis.z;
            float oneMinusCosByZX = oneMinusCos * axis.z * axis.x;
            float xSin = sin * axis.x;
            float ySin = sin * axis.y;
            float zSin = sin * axis.z;

            return new Vector3
            {
                x = value.x * (cos + oneMinusCos * axis.x * axis.x) + value.y * (oneMinusCosByXY - zSin) + value.z * (oneMinusCosByZX + ySin),
                y = value.x * (oneMinusCosByXY + zSin) + value.y * (cos + oneMinusCos * axis.y * axis.y) + value.z * (oneMinusCosByYZ - xSin),
                z = value.x * (oneMinusCosByZX - ySin) + value.y * (oneMinusCosByYZ + xSin) + value.z * (cos + oneMinusCos * axis.z * axis.z)
            };
        }

        //--//

        /// <summary>
        /// Returns vector2 value clamped between values represented by the bounds.
        /// </summary>
        public static Vector2 GetClamped(this Vector2 value, Rect bounds)
        {
            return new Vector2
            {
                x = value.x.Clamp(bounds.xMin, bounds.xMax),
                y = value.y.Clamp(bounds.yMin, bounds.yMax)
            };
        }

        /// <summary>
        /// Returns a copy of vector with its magnitude clamped to maxLength.
        /// </summary>
        public static Vector2 GetClamped(this Vector2 value, float maxLength)
        {
            return Vector2.ClampMagnitude(value, maxLength);
        }

        /// <summary>
        /// Returns a copy of vector with its magnitude clamped to maxLength.
        /// </summary>
        public static Vector3 GetClamped(this Vector3 value, float maxLength)
        {
            return Vector3.ClampMagnitude(value, maxLength);
        }

        /// <summary>
        /// Is position in bounds.
        /// </summary>
        /// <param name="bounds">Checked bounds</param>
        public static bool InBounds(this Vector2 pos, Rect bounds)
        {
            return bounds.Contains(pos);
        }

        /// <summary>
        /// Is position in bounds.
        /// </summary>
        /// <param name="bounds">Checked bounds</param>
        public static bool InBounds(this Vector3 pos, Rect bounds)
        {
            return bounds.Contains(pos);
        }

        /// <summary>
        /// Returns the aspect ratio of the rect (height / width).
        /// </summary>
        public static float GetAspectRatio(this Rect value)
        {
            return value.height / value.width;
        }

        /// <summary>
        /// Returns the diagonal length of the rect.
        /// </summary>
        public static float GetDiagonal(this Rect value)
        {
            float w = value.width;
            float h = value.height;
            return (w * w + h * h).Sqrt();
        }

        /// <summary>
        /// Returns pivot in zero coordinates relative to the rect.
        /// </summary>
        public static Vector2 GetPivot(this Rect rect)
        {
            float x = (0f - rect.xMin) / rect.width;
            float y = (0f - rect.yMin) / rect.height;
            return new Vector2(x, y);
        }

        /// <summary>
        /// Returns rect expanded multiplicatively relative to zero coordinates.
        /// </summary>
        public static Rect GetMultiplied(this Rect value, Vector2 expandFactor)
        {
            return GetMultiplied(value, expandFactor.x, expandFactor.y);
        }

        /// <summary>
        /// Returns rect expanded multiplicatively relative to zero coordinates.
        /// </summary>
        public static Rect GetMultiplied(this Rect value, float xFactor, float yFactor)
        {
            value.xMin *= xFactor;
            value.yMin *= yFactor;

            value.xMax *= xFactor;
            value.yMax *= yFactor;

            return value;
        }

        /// <summary>
        /// Returns rect expanded relative to pivot.
        /// </summary>
        /// <param name="expandPivot">Relative point of the rect from (0, 0) to (1, 1).</param>
        /// <param name="multiply">Uses multiplication for expansion if true, else uses addition.</param>
        public static Rect GetExpanded(this Rect value, Vector2 expandSize, Vector2 expandPivot, bool multiply = false)
        {
            Vector2 pos = value.position + Vector2.Scale(value.size, expandPivot);

            if (multiply)
                value.size = Vector2.Scale(value.size, expandSize);
            else
                value.size += expandSize;

            value.position = pos - Vector2.Scale(value.size, expandPivot);

            return value;
        }

        /// <summary>
        /// Returns additively expanded rect without preserving pivot.
        /// </summary>        
        public static Rect GetExpanded(this Rect value, Vector2 expandSize)
        {
            value.size += expandSize;
            value.position -= expandSize * 0.5f;

            return value;
        }

        public static Rect ToRect(this RectInt value)
        {
            return new Rect(value.xMin, value.yMin, value.width, value.height);
        }

        public static Rect ToRect(this RectInt value, Vector2 expandFactor)
        {
            return new Rect(value.xMin * expandFactor.x, value.yMin * expandFactor.y, value.width * expandFactor.x, value.height * expandFactor.y);
        }

        public static Rect ToRect(this RectInt value, float xFactor, float yFactor)
        {
            return new Rect(value.xMin * xFactor, value.yMin * yFactor, value.width * xFactor, value.height * yFactor);
        }

        //--//

        public static bool HasLayer(this LayerMask mask, int layer)
        {
            return (mask & 1 << layer) != 0;
        }

        public static bool HasLayer(this LayerMask mask, string layer)
        {
            return (mask & 1 << LayerMask.NameToLayer(layer)) != 0;
        }
    }
}
