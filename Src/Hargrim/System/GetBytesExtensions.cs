﻿namespace System
{
    public static class GetBytesExtensions
    {
        /// <summary>
        /// Returns the value as an array of bytes.
        /// </summary>
        public static byte[] GetBytes(this char value) { return BitConverter.GetBytes(value); }

        /// <summary>
        /// Returns the value as an array of bytes.
        /// </summary>
        public static byte[] GetBytes(this short value) { return BitConverter.GetBytes(value); }

        /// <summary>
        /// Returns the value as an array of bytes.
        /// </summary>
        public static byte[] GetBytes(this int value) { return BitConverter.GetBytes(value); }

        /// <summary>
        /// Returns the value as an array of bytes.
        /// </summary>
        public static byte[] GetBytes(this long value) { return BitConverter.GetBytes(value); }

        /// <summary>
        /// Returns the value as an array of bytes.
        /// </summary>
        public static byte[] GetBytes(this ushort value) { return BitConverter.GetBytes(value); }

        /// <summary>
        /// Returns the value as an array of bytes.
        /// </summary>
        public static byte[] GetBytes(this uint value) { return BitConverter.GetBytes(value); }

        /// <summary>
        /// Returns the value as an array of bytes.
        /// </summary>
        public static byte[] GetBytes(this ulong value) { return BitConverter.GetBytes(value); }

        /// <summary>
        /// Returns the value as an array of bytes.
        /// </summary>
        public static byte[] GetBytes(this float value) { return BitConverter.GetBytes(value); }

        /// <summary>
        /// Returns the value as an array of bytes.
        /// </summary>
        public static byte[] GetBytes(this double value) { return BitConverter.GetBytes(value); }
    }
}
