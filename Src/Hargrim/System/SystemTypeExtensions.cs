﻿using System.Reflection;
using Hargrim;

namespace System
{
    public static class SystemTypeExtensions
    {
        /// <summary>
        /// Retrieves the name of the constant in the specified enumeration that has the specified value.
        /// </summary>        
        public static string GetName(this Enum value)
        {
            return Enum.GetName(value.GetType(), value);
        }

        /// <summary>
        /// Indicates whether the string is not null and not an empty string.
        /// </summary>
        public static bool HasAnyData(this string str)
        {
            return !string.IsNullOrEmpty(str);
        }

        /// <summary>
        /// Returns integer value of the enumeration.
        /// </summary>        
        public static int ToInt(this Enum value)
        {
            TypeCode typeCode = value.GetTypeCode();

            switch (typeCode)
            {
                case TypeCode.SByte: return (sbyte)(object)value;
                case TypeCode.Byte: return (byte)(object)value;
                case TypeCode.Int16: return (short)(object)value;
                case TypeCode.UInt16: return (ushort)(object)value;
                case TypeCode.Int32: return (int)(object)value;
                case TypeCode.UInt32: return (int)(uint)(object)value;
                case TypeCode.Int64: return (int)(long)(object)value;
                case TypeCode.UInt64: return (int)(ulong)(object)value;

                default: throw new UnsupportedValueException(typeCode);
            }
        }

#if DOT_NET_3_5

        /// <summary>
        ///Indicates whether the string is not null, empty, and doesn't consist only of white-space characters.
        /// </summary>
        public static bool HasUsefulData(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return false;
            }

            for (int i = 0; i < str.Length; i++)
            {
                if (!char.IsWhiteSpace(str[i]))
                {
                    return true;
                }
            }

            return false;
        }

#elif DOT_NET_4_6

        /// <summary>
        ///Indicates whether the string is not null, empty, and doesn't consist only of white-space characters.
        /// </summary>
        public static bool HasUsefulData(this string str)
        {
            return !string.IsNullOrWhiteSpace(str);
        }

#endif

        public static TypeCode GetTypeCode(this Type type)
        {
            return Type.GetTypeCode(type);
        }

        public static bool Is(this Type type, TypeCode typeCode)
        {
            return Type.GetTypeCode(type) == typeCode;
        }

        public static object FullClone(this object sourceObj)
        {
            if (sourceObj == null)
                return null;

            Type type = sourceObj.GetType();

            if (type.Is(TypeCode.Object))
            {
                if (type.IsArray)
                    return f_cloneArray(sourceObj, type);

                object destObj = Activator.CreateInstance(sourceObj.GetType(), true);

                FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

                for (int i = 0; i < fields.Length; i++)
                {
                    object fieldValue = fields[i].GetValue(sourceObj);
                    if (fieldValue == null) { continue; }
                    fields[i].SetValue(destObj, FullClone(fieldValue));
                }

                return destObj;
            }

            return sourceObj;
        }

        private static object f_cloneArray(object source, Type t)
        {
            Array sourceArray = source as Array;
            Type elementType = t.GetElementType();

            if (elementType.Is(TypeCode.Object))
            {
                Array destArray = Array.CreateInstance(elementType, sourceArray.Length);

                object clonedElement;
                for (int i = 0; i < sourceArray.Length; i++)
                {
                    clonedElement = FullClone(sourceArray.GetValue(i));
                    destArray.SetValue(clonedElement, i);
                }

                return destArray;
            }

            return sourceArray.Clone();
        }
    }
}
