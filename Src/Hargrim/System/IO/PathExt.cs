﻿namespace System.IO
{
    public static class PathExt
    {
        public static string GetParentPath(string path, int steps = 1)
        {
            return f_getParentPath(path, steps, f_separatorChecker);
        }

        public static string GetParentPath(string path, char separator, int steps = 1)
        {
            return f_getParentPath(path, steps, ch => ch == separator);
        }

        // -- //

        private static string f_getParentPath(string path, int steps, Func<char, bool> separatorChecker)
        {
            string parent = path;

            for (int i = 0; i < steps; i++)
            {
                bool terminator = true;
                for (int j = parent.Length - 1; j >= 0; j--)
                {
                    if (separatorChecker(parent[j]))
                    {
                        if (!terminator) { parent = parent.Substring(0, j); break; }
                    }
                    else { terminator = false; }
                }
            }

            return parent;
        }

        private static bool f_separatorChecker(char ch)
        {
            return ch == Path.DirectorySeparatorChar || ch == Path.AltDirectorySeparatorChar;
        }
    }

    public static class IOExtensions
    {
        public static string GetParentPath(this DirectoryInfo dir, int steps = 1)
        {
            return PathExt.GetParentPath(dir.FullName, steps);
        }

        public static string GetParentPath(this FileInfo file, int steps = 1)
        {
            return PathExt.GetParentPath(file.FullName, steps);
        }
    }
}
