﻿namespace System
{
    public static class EnumExt
    {
#if DOT_NET_3_5

        /// <summary>Converts the string representation of the name or numeric value of one or more enumerated constants to an equivalent enumerated object. A parameter specifies whether the operation is case-sensitive. The return value indicates whether the conversion succeeded.</summary>
        /// <param name="value">The string representation of the enumeration name or underlying value to convert.</param>        
        /// <param name="result">When this method returns, contains an object of type <typeparamref name="TEnum" /> whose value is represented by <paramref name="value" />. This parameter is passed uninitialized.</param>
        /// <typeparam name="TEnum">The enumeration type to which to convert <paramref name="value" />.</typeparam>
        /// <returns>true if the <paramref name="value" /> parameter was converted successfully; otherwise, false.</returns>
        public static bool TryParse<TEnum>(string value, out TEnum result) where TEnum : struct
        {
            try
            {
                result = (TEnum)Enum.Parse(typeof(TEnum), value);
                return true;
            }
            catch (Exception)
            {
                result = default(TEnum);
                return false;
            }
        }

        /// <summary>Converts the string representation of the name or numeric value of one or more enumerated constants to an equivalent enumerated object. A parameter specifies whether the operation is case-sensitive. The return value indicates whether the conversion succeeded.</summary>
        /// <param name="value">The string representation of the enumeration name or underlying value to convert.</param>
        /// <param name="ignoreCase">true to ignore case; false to consider case.</param>
        /// <param name="result">When this method returns, contains an object of type <typeparamref name="TEnum" /> whose value is represented by <paramref name="value" />. This parameter is passed uninitialized.</param>
        /// <typeparam name="TEnum">The enumeration type to which to convert <paramref name="value" />.</typeparam>
        /// <returns>true if the <paramref name="value" /> parameter was converted successfully; otherwise, false.</returns>
        public static bool TryParse<TEnum>(string value, bool ignoreCase, out TEnum result) where TEnum : struct
        {
            try
            {
                result = (TEnum)Enum.Parse(typeof(TEnum), value, ignoreCase);
                return true;
            }
            catch (Exception)
            {
                result = default(TEnum);
                return false;
            }
        }

#elif DOT_NET_4_6

        [Obsolete("This function is deprecated. Use Enum.TryParse instead.")]
        public static bool TryParse<TEnum>(string value, out TEnum result) where TEnum : struct
        {
            return Enum.TryParse(value, out result);
        }

        [Obsolete("This function is deprecated. Use Enum.TryParse instead.")]
        public static bool TryParse<TEnum>(string value, bool ignoreCase, out TEnum result) where TEnum : struct
        {
            return Enum.TryParse(value, ignoreCase, out result);
        }

#endif
    }
}
