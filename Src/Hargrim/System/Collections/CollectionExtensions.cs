﻿using Hargrim.Collections;
using Hargrim.MathExt;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Collections
{
    public static class CollectionExtensions
    {
        [StructLayout(LayoutKind.Explicit)]
        private struct IntToLong
        {
            [FieldOffset(0)]
            public long Long;
            [FieldOffset(0)]
            public int Int0;
            [FieldOffset(4)]
            public int Int1;
        }

        private static FieldInfo m_field;

        static CollectionExtensions()
        {
            m_field = typeof(BitArray).GetField("m_array", BindingFlags.Instance | BindingFlags.NonPublic);
        }

        /// <summary>
        /// Returns true if any bit is true.
        /// </summary>
        public static bool Any(this BitArray bitArray)
        {
            if (bitArray.Length.IsPoT())
            {
                int[] array = m_field.GetValue(bitArray) as int[];

                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] != 0) { return true; }
                }
            }
            else
            {
                for (int i = 0; i < bitArray.Length; i++)
                {
                    if (bitArray[i]) { return true; }
                }
            }

            return false;
        }

        /// <summary>
        /// Returns true if all bits is true.
        /// </summary>
        public static bool All(this BitArray bitArray)
        {
            bool ku = true;

            if (bitArray.Length.IsPoT())
            {
                int[] array = m_field.GetValue(bitArray) as int[];

                for (int i = 0; i < array.Length; i++)
                {
                    ku &= array[i] == -1;
                }
            }
            else
            {
                for (int i = 0; i < bitArray.Length; i++)
                {
                    ku &= bitArray[i];
                }
            }

            return ku;
        }

        public static BitMask ToBitMask(this BitArray bitArray, bool throwOutOfRange = true)
        {
            if (throwOutOfRange && bitArray.Count > BitMask.SIZE)
                throw new ArgumentOutOfRangeException("bits", "Given bit array has length more than " + BitMask.SIZE.ToString());

            //BitMask mask = new BitMask();

            //for (int i = 0; i < bitArray.Length; i++)
            //{
            //    if (bitArray[i]) { mask.AddFlag(i); }
            //}

            //return mask;

            int[] array = m_field.GetValue(bitArray) as int[];

            return array.Length > 0 ? array[0] : 0;
        }

        public static BitMask64 ToBitMask64(this BitArray bitArray, bool throwOutOfRange = true)
        {
            if (throwOutOfRange && bitArray.Count > BitMask64.SIZE)
                throw new ArgumentOutOfRangeException("bits", "Given bit array has length more than " + BitMask64.SIZE.ToString());

            //BitMask64 mask = new BitMask64();

            //for (int i = 0; i < bitArray.Length; i++)
            //{
            //    if (bitArray[i]) { mask.AddFlag(i); }
            //}

            //return mask;

            int[] array = m_field.GetValue(bitArray) as int[];

            if (array.Length == 0)
            {
                return 0L;
            }
            else if (array.Length == 1)
            {
                return array[0];
            }
            else
            {
                IntToLong intToLong = new IntToLong
                {
                    Int0 = array[0],
                    Int1 = array[1]
                };

                return intToLong.Long;
            }
        }
    }
}
